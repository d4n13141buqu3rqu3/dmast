require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module Dmast
  class Application < Rails::Application
    #config.assets.paths += Dir["#{Rails.root}/vendor/Content/**/"].sort_by {|dir| -dir.size}
    #config.assets.paths += Dir["#{Rails.root}/vendor/downloads/**/"].sort_by {|dir| -dir.size}
    config.autoload_paths << "#{Rails.root}/app/repositories"

    #Configuração MAILERS

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
        :address              => "smtp.gmail.com",
        :port                 => 587,
        :domain               => 'gmail.com',
        :user_name            => 'workflow@amcel.com.br',
        :password             => 'p@55word',
        :authentication       => 'plain',
        :enable_starttls_auto => true
    }
    # Para debug apenas, é melhor que a linha abaixo seja adicionado apenas no ambiente de desenvolvimento
    config.action_mailer.raise_delivery_errors = true

    
  end
end
