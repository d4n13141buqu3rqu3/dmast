OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do

	conf_prod = {
			chave: 'XZI1kO5qpotm7MIa-jq9FzL2',
			id: '518609120217-c0fda63b4630hs3to1u8i42lfd464ho7.apps.googleusercontent.com'
	}


	conf_dev = {
			chave: 'XxHD6c0hY9Ow3F1aEXKdF3Uo',
			id: '646299117440-lgll7lh28bjs6rr82etn6mud38ehqkla.apps.googleusercontent.com'
	}


	if Rails.env == "production"
		chave = conf_prod[:chave]; id = conf_prod[:id]
	else
		chave = conf_dev[:chave]; id = conf_dev[:id]
	end


	provider :google_oauth2, id, chave,
  	{
  		client_options:
  		{
  			:scope => "email, profile",
  			:prompt => "select_account",
  			:image_aspect_ratio => "square",
      		:image_size => 50,
  			ssl: {
  				ca_file: Rails.root.join("cacert.pem").to_s
  			}
  		}
  	}
end
