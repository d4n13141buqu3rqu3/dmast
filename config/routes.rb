require 'sidekiq/web'
require 'sidetiq/web'

Rails.application.routes.draw do

  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == 'admin' && password == 'admin'
  end



  mount Sidekiq::Web, at: '/sidekiq'

  resources :filiais

  get 'demais_racs/index'

  resources :helpers

  resources :demais_racs do
    member do
      get :atualizar
    end
  end

  resources :grp_consensadores
  resources :grp_homologadores

  resources :followups

  resources :documento_localizacoes

  get 'anexo_documentos/new'

  get 'anexo_documentos/create'

  get 'anexo_documentos/anexo_params'

  resources :anexo_docs

  resources :consensadores

  resources :homologadores

  resources :consensamentos

  resources :documentos do
    member do

      get :novo



    end
  end

  get 'anexo_reunioes/new'

  get 'anexo_reunioes/create'

  resources :topicos

  resources :locais

  resources :raps do
    member do
      get :atualizar
      get :alterar_data_acao
    end
  end

  resources :racs do
    member do
      get :atualizar
      get :alterar_data_acao
    end
  end

  resources :rais do
    member do
      get :atualizar
    end
  end

  resources :reunioes do
    member do
      get :atualizar
    end
  end

  resources :planosauditorias do
    member do
      get :atualizar
    end
  end

  resources :funcionarios

  get 'grupos/index'

  resources :tipo_racs do
    member do
      get :atualizar
    end
  end



  resources :tipo_docs do
    member do
      get :atualizar
    end
  end

  resources :tipo_ncs do
    member do
      get :atualizar
    end
  end

  resources :areas do
    member do
      get :atualizar
    end
  end

  resources :pastas do
    member do
      get :atualizar
    end
  end  

  resources :numeros do
    member do
      get :atualizar
    end
  end  

  resources :localizacoes do
    member do
      get :atualizar
    end
  end  

  resources :tipo_atas do
    member do
      get :atualizar
    end
  end

  resources :funcionarios do
    member do
      get :atualizar
    end
  end

  resources :auditorias do
    member do
      get :atualizar
      get :imprimir
      get :visualizar
    end
  end

  resource :anexo_reunioes do
    member do
      get :salvar
    end
  end

  get 'notificacoes/visto'

  get 'notificacoes/listar'

  get 'sessions/create'
  get 'sessions/destroy'

  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')

  get 'sair', to: 'sessions#destroy', as: 'signout'
  get 'sessions/nao_autorizado'

  get 'home/index'
  get 'notificacoes/pesquisar'

  match 'ajax_planos', controller: 'planosauditorias', action: 'ajax_planos', via: 'get'
  match 'ajax_ajuda', controller: 'helpers', action: 'ajax_ajuda', via: 'get'

  match 'editar-grupo-consenso-:id', controller: 'grp_consensadores', action: 'edit', via: 'get'
  match 'editar-grupo-consenso', controller: 'grp_consensadores', action: 'update', via: 'post'
  match 'editar-grupo-consenso', controller: 'grp_consensadores', action: 'update', via: 'patch'

  match 'editar-grupo-homologacao-:id', controller: 'grp_homologadores', action: 'edit', via: 'get'
  match 'editar-grupo-homologacao', controller: 'grp_homologadores', action: 'update', via: 'post'
  match 'editar-grupo-homologacao', controller: 'grp_homologadores', action: 'update', via: 'patch'

  match 'visualizar-documentos-:filtro', controller: 'documentos', action: 'index', via: 'get'
  match 'imprimir-rac-:id', controller: 'racs', action: 'print', via: 'get'
  match 'imprimir-rai-:id', controller: 'rais', action: 'print', via: 'get'
  match 'imprimir-rap-:id', controller: 'raps', action: 'print', via: 'get'
  match 'imprimir-demais-rac-:id', controller: 'demais_racs', action: 'print', via: 'get'
  match 'visualizar-documento-:id', controller: 'documentos', action: 'visualizar', via: 'get'
  match 'documentos-excluidos', controller: 'documentos', action: 'lixeira', via: 'get'
  match 'consenso-documento-:id', controller: 'documentos', action: 'visualizar_via_email', via: 'get'

  match 'racs', controller: 'racs', action: 'index', via: 'get'


  match 'racs,visualizar,:filtro', controller: 'racs', action: 'lista', via: 'get'

  match 'enviar-doc-lixeira', controller: 'documentos', action: 'enviar_lixeira', via: 'post'
  match 'restaurar-doc-lixeira', controller: 'documentos', action: 'restaurar_lixeira', via: 'post'

  match 'demais-racs', controller: 'demais_racs', action: 'index', via: 'get'
  match 'inserir-demais-racs', controller: 'demais_racs', action: 'inserir', via: 'get'
  match 'raps', controller: 'raps', action: 'lista', via: 'get'

  match 'backup', controller: 'backup', action: 'do', via: 'get'

  match 'lista-auditorias-:id', controller: 'auditorias', action: 'lista', via: 'get'
  match 'visualizar-auditoria-:id', controller: 'auditorias', action: 'show', via: 'get'
  match 'download-anexo-:id', controller: 'reunioes', action: 'download_anexo', via: 'get'
  match 'doc-download-anexo-:id', controller: 'documentos', action: 'download_anexo', via: 'get'
  match 'visualizar-notificacao-:id', controller: 'notificacoes', action: 'visualizar', via: 'get'
  match 'anexar-arquivo-na-reuniao-:id', controller: 'reunioes', action: 'anexar', via: 'get'
  match 'adicionar-anexo-doc-:doc_id', controller: 'documentos', action: 'anexar', via: 'get'


  match 'adiamentos,rac,:id', controller: 'racs', action: 'ver_adiamentos', via: 'get'
  match 'gerenciar-ajuda', controller: 'helpers', action: 'index', via: 'get'

  match 'lista-de-racs', controller: 'racs', action: 'index', via: 'get'

  get 'ajax-salva-topico/:reuniao_id/:descricao', controller: 'reunioes', action: 'salva_topico', via: 'get'
  get 'encerrar-reuniao-:id', controller: 'reunioes', action: 'encerrar_reuniao', via: 'get'

  match 'relatorios-de-auditorias-internas', controller: 'rais', action: 'index', via: 'get'

  match 'auditorias,plano,:id', controller: 'auditorias', action: 'index', via: 'get'

  match 'marcar,como,finalizada,:id', controller: 'reunioes', action: 'marcar_finalizada', via: 'get'
  match 'excluir,topico,:id', controller:'reunioes', action: 'excluir_topico', via: 'get'

  match 'areas-de-atuacao', controller: 'areas', action: 'index', via: 'get'
  match 'ver-notificacoes', controller: 'notificacoes', action: 'listar', via: 'get'
  match 'tipos-de-atas', controller: 'tipo_atas', action: 'index', via: 'get'
  match 'tipos-de-ncs', controller: 'tipo_ncs', action: 'index', via: 'get'
  match 'tipos-de-docs', controller: 'tipo_docs', action: 'index', via: 'get'
  match 'tipos-de-racs', controller: 'tipo_racs', action: 'index', via: 'get'

  match 'planos-de-auditorias', controller: 'planosauditorias', action: 'index', via: 'get'

  match 'reunioes', controller: 'reunioes', action: 'index', via: 'get'

  match 'nova-reuniao', controller: 'reunioes', action: 'incluir', via: 'get'

  match 'rqa', controller: 'rqas', action: 'incluir', via: 'get'

  match 'lista-de-funcionarios', controller: 'funcionarios', action: 'index', via: 'get'
  match 'lista-de-funcionarios-2', controller: 'funcionarios', action: 'lista_funcionarios', via: 'get'

  match 'grupos-de-usuarios', controller: 'grupos', action: 'index', via: 'get'

  match 'cadastrar-nova-rai', controller: 'rais', action: 'new', via: 'get'
  match 'cadastrar-nova-rac', controller: 'racs', action: 'new', via: 'get'
  match 'cadastrar-nova-rap', controller: 'raps', action: 'new', via: 'get'
  match 'elaborar-documento', controller: 'documentos', action: 'new', via: 'get'


  match 'lista-de-raps', controller: 'raps', action: 'index', via: 'get'
  match 'ajax-lista-auditorias-:id', controller: 'auditorias', action: 'lista', via: 'get'
  match 'ajax-novo-topico-:reuniao_id', controller: 'topicos', action: 'add', via: 'get'

  match 'destinatarios-do-plano-auditoria,:id', controller: 'planosauditorias', action: 'destinatarios', via: 'get'
  match 'notificar-destinatarios-do-plano,:id', controller: 'planosauditorias', action: 'notificar_destinatarios', via: 'get'
  match 'apagar,auditoria,:id', controller: 'auditorias', action: 'delete', via: 'get'

  match 'grupo-consensadores', controller: 'grp_consensadores', action: 'index', via: 'get'
  match 'grupo-homologadores', controller: 'grp_homologadores', action: 'index', via: 'get'

  match 'iniciar-workflow-:id', controller: 'documentos', action: 'consensar', via: 'get'
  match 'enviar-para-consenso-:id', controller: 'documentos', action: 'enviar_consenso', via: 'get'

  post 'recebe-resposta-consenso', controller: 'documentos', action: 'recebe_resposta_consenso', via: 'post'
  get 'rqas', controller: 'rqas', action: 'index', via: 'get'
  post 'salva-rqa', controller: 'rqas', action: 'salvar', via: 'post'

  get 'confirmar-consenso-:id', controller: 'documentos', action: 'recebe_resposta_consenso', via: 'get'

  get 'homologar-documento-:id', controller: 'documentos', action: 'homologar', via: 'get'

  post 'recebe-resposta-homologacao', controller: 'documentos', action: 'recebe_resposta_homologacao', via: 'post'

  get 'confirmar-homologacao-:id', controller: 'documentos', action: 'recebe_resposta_homologacao', via: 'get'

  get 'add-atv-auditoria-:id', controller: 'auditorias', action: 'add_atividade', via: 'get'
  get 'del-atv-:id', controller: 'auditorias', action: 'del_atividade', via: 'get'

  post 'salva-atividade', controller: 'auditorias', action: 'salva_atividade', via: 'post'

  get 'follow-up-auditoria-:id', controller: 'followups', action: 'new', via: 'post'

  post '/tinymce_assets' => 'uploadimg#create'

  get 'atualizar-documento-:id', controller: 'documentos', action: 'edit', via: 'post'


  get 'adicionar-localizacao-doc-:id', controller: 'documento_localizacoes', action: 'new', via: 'get'

  get 'add-destinatarios', controller: 'followups', action: 'add_destinatario', via: 'post'

  get 'disparar-followup-:id', controller: 'auditorias', action: 'disparar_followup', via: 'post'

  get 'documento-:id', controller: 'documentos', action: 'ver', via: 'get'

  get 'doc-complementar-:id', controller: 'documentos', action: 'add_doc_complementar', via: 'get'

  post 'doc-complementar', controller: 'documentos', action: 'add_doc_complementar', via: 'post'

  get 'oficial-documento-:id', controller: 'documentos', action: 'ver_oficial', via: 'get'

  get 'criar-versao-documento-:id', controller: 'documentos', action: 'criar_versao', via: 'get'




  mount API::Base => '/api'






  resources :sessions, only: [:create, :destroy]
  resource :home, only: [:show]

  post '/tinymce_assets' => 'tinymce_assets#create'

  match 'rai-:id-followup', controller: 'rais', action: 'followup_lista', via: 'get'
  match 'salvar-follow-up-rai', controller: 'rais', action: "followup_salvar", via: 'post'
  match 'remove-follow-up-rai-:id', controller: 'rais', action: "followup_remove", via: 'get'
  match 'atualiza-follow-up-rai-:id', controller: 'rais', action: "followup_atualiza", via: 'get'
  match 'atualiza-follow-up-rai', controller: 'rais', action: "followup_atualiza_salva", via: 'post'

  match 'rac-:id-followup', controller: 'racs', action: 'followup_lista', via: 'get'
  match 'salvar-follow-up-rac', controller: 'racs', action: "followup_salvar", via: 'post'
  match 'remove-follow-up-rac-:id', controller: 'racs', action: "followup_remove", via: 'get'
  match 'atualiza-follow-up-rac-:id', controller: 'racs', action: "followup_atualiza", via: 'get'
  match 'atualiza-follow-up-rac', controller: 'racs', action: "followup_atualiza_salva", via: 'post'

  match 'rap-:id-followup', controller: 'raps', action: 'followup_lista', via: 'get'
  match 'salvar-follow-up-rap', controller: 'raps', action: "followup_salvar", via: 'post'
  match 'remove-follow-up-rap-:id', controller: 'raps', action: "followup_remove", via: 'get'
  match 'atualiza-follow-up-rap-:id', controller: 'raps', action: "followup_atualiza", via: 'get'
  match 'atualiza-follow-up-rap', controller: 'raps', action: "followup_atualiza_salva", via: 'post'


  #Reenvio de workflow
  match 'reenviar-workflow-:id', controller: 'documentos', action: 'reenviar_workflow', via: 'get'
  match 'reenviar-workflow', controller: 'documentos', action: 'reenviar_workflow', via: 'post'

  root to: "home#index"
end

