lock '3.1.0'

set :application, 'dmast'
set :repo_url, 'https://d4n13141buqu3rqu3@bitbucket.org/d4n13141buqu3rqu3/dmast.git'
#set :repo_url, 'git@bitbucket.org:d4n13141buqu3rqu3/dmast.git'
set :deploy_to, "/home/amcel/dmast"
set :linked_files, fetch(:linked_files, []).push('config/database.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('bin', 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')
set :keep_releases, 3
set :scm, :git
set :rvm_type, :user
set :rvm_ruby_version, '2.2.0'
set :log_level, :debug


desc "Check if agent forwarding is working"
task :forwarding do
  on roles(:all) do |h|
    if test("env | grep SSH_AUTH_SOCK")
      info "Agent forwarding is up to #{h}"
    else
      error "Agent forwarding is NOT up to #{h}"
    end
  end
end

namespace :deploy do

  desc 'Create uploads symlink folder'
  task :create_symlink do
    on roles(:all) do |h|
      execute "ln -s /home/amcel/dmast/static/uploads /home/amcel/dmast/releases/#{File.basename release_path}/public"
      execute "ln -s /home/amcel/dmast/static/anexos /home/amcel/dmast/releases/#{File.basename release_path}/public"
      #execute "ln -s /home/amcel/dmast/static/system /home/amcel/dmast/releases/#{File.basename release_path}/public"
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
    execute :touch, release_path.join('tmp/restart.txt')
  end
  end

after :deploy, "deploy:create_symlink"
after :publishing, 'deploy:restart'
after :finishing, 'deploy:cleanup'
end
