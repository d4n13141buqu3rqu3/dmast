require 'test_helper'

class RacsControllerTest < ActionController::TestCase
  setup do
    @rac = racs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:racs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rac" do
    assert_difference('Rac.count') do
      post :create, rac: { analise_causa: @rac.analise_causa, analise_eficacia: @rac.analise_eficacia, area_id: @rac.area_id, auditoria_id: @rac.auditoria_id, desc_correacao: @rac.desc_correacao, dt_prevista_correacao: @rac.dt_prevista_correacao, dt_real_encerramento: @rac.dt_real_encerramento, dt_real_execucao: @rac.dt_real_execucao, justificativa: @rac.justificativa, nova_dt_prevista: @rac.nova_dt_prevista, responsavel: @rac.responsavel, responsavel_correcao: @rac.responsavel_correcao, status: @rac.status, status_correcao: @rac.status_correcao, tipo_rac_id: @rac.tipo_rac_id, usuario_id: @rac.usuario_id }
    end

    assert_redirected_to rac_path(assigns(:rac))
  end

  test "should show rac" do
    get :show, id: @rac
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rac
    assert_response :success
  end

  test "should update rac" do
    patch :update, id: @rac, rac: { analise_causa: @rac.analise_causa, analise_eficacia: @rac.analise_eficacia, area_id: @rac.area_id, auditoria_id: @rac.auditoria_id, desc_correacao: @rac.desc_correacao, dt_prevista_correacao: @rac.dt_prevista_correacao, dt_real_encerramento: @rac.dt_real_encerramento, dt_real_execucao: @rac.dt_real_execucao, justificativa: @rac.justificativa, nova_dt_prevista: @rac.nova_dt_prevista, responsavel: @rac.responsavel, responsavel_correcao: @rac.responsavel_correcao, status: @rac.status, status_correcao: @rac.status_correcao, tipo_rac_id: @rac.tipo_rac_id, usuario_id: @rac.usuario_id }
    assert_redirected_to rac_path(assigns(:rac))
  end

  test "should destroy rac" do
    assert_difference('Rac.count', -1) do
      delete :destroy, id: @rac
    end

    assert_redirected_to racs_path
  end
end
