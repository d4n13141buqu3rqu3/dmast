require 'test_helper'

class PlanosauditoriasControllerTest < ActionController::TestCase
  setup do
    @planoauditoria = planosauditorias(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:planosauditorias)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create planoauditoria" do
    assert_difference('Planoauditoria.count') do
      post :create, planoauditoria: { ano: @planoauditoria.ano, encerrado: @planoauditoria.encerrado, nome: @planoauditoria.nome, prazo: @planoauditoria.prazo }
    end

    assert_redirected_to planoauditoria_path(assigns(:planoauditoria))
  end

  test "should show planoauditoria" do
    get :show, id: @planoauditoria
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @planoauditoria
    assert_response :success
  end

  test "should update planoauditoria" do
    patch :update, id: @planoauditoria, planoauditoria: { ano: @planoauditoria.ano, encerrado: @planoauditoria.encerrado, nome: @planoauditoria.nome, prazo: @planoauditoria.prazo }
    assert_redirected_to planoauditoria_path(assigns(:planoauditoria))
  end

  test "should destroy planoauditoria" do
    assert_difference('Planoauditoria.count', -1) do
      delete :destroy, id: @planoauditoria
    end

    assert_redirected_to planosauditorias_path
  end
end
