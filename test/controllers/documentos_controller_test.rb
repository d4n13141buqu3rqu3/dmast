require 'test_helper'

class DocumentosControllerTest < ActionController::TestCase
  setup do
    @documento = documentos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:documentos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create documento" do
    assert_difference('Documento.count') do
      post :create, documento: { dias_consenso: @documento.dias_consenso, dias_homologar: @documento.dias_homologar, dt_envio_consenso: @documento.dt_envio_consenso, dt_envio_homologacao: @documento.dt_envio_homologacao, id_doc_anterior: @documento.id_doc_anterior, link_versao_anterior: @documento.link_versao_anterior, modificado_por_id: @documento.modificado_por_id, nome: @documento.nome, nome_original: @documento.nome_original, sigla: @documento.sigla, status: @documento.status, texto_doc: @documento.texto_doc, tipoDoc_id: @documento.tipoDoc_id, titulo: @documento.titulo, usuario_id: @documento.usuario_id, versao: @documento.versao, versao_anterior: @documento.versao_anterior }
    end

    assert_redirected_to documento_path(assigns(:documento))
  end

  test "should show documento" do
    get :show, id: @documento
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @documento
    assert_response :success
  end

  test "should update documento" do
    patch :update, id: @documento, documento: { dias_consenso: @documento.dias_consenso, dias_homologar: @documento.dias_homologar, dt_envio_consenso: @documento.dt_envio_consenso, dt_envio_homologacao: @documento.dt_envio_homologacao, id_doc_anterior: @documento.id_doc_anterior, link_versao_anterior: @documento.link_versao_anterior, modificado_por_id: @documento.modificado_por_id, nome: @documento.nome, nome_original: @documento.nome_original, sigla: @documento.sigla, status: @documento.status, texto_doc: @documento.texto_doc, tipoDoc_id: @documento.tipoDoc_id, titulo: @documento.titulo, usuario_id: @documento.usuario_id, versao: @documento.versao, versao_anterior: @documento.versao_anterior }
    assert_redirected_to documento_path(assigns(:documento))
  end

  test "should destroy documento" do
    assert_difference('Documento.count', -1) do
      delete :destroy, id: @documento
    end

    assert_redirected_to documentos_path
  end
end
