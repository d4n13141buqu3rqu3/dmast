require 'test_helper'

class RapsControllerTest < ActionController::TestCase
  setup do
    @rap = raps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:raps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rap" do
    assert_difference('Rap.count') do
      post :create, rap: { analise_critica: @rap.analise_critica, area_responsavel_id: @rap.area_responsavel_id, area_solicitante_id: @rap.area_solicitante_id, consequencia: @rap.consequencia, desc_acao: @rap.desc_acao, desc_nc: @rap.desc_nc, dt_encerramento: @rap.dt_encerramento, dt_prevista: @rap.dt_prevista, justificativa: @rap.justificativa, nova_dt: @rap.nova_dt, solicitante: @rap.solicitante, status: @rap.status, status_acao: @rap.status_acao, tipo_nc_id: @rap.tipo_nc_id, tipo_reclamacao_id: @rap.tipo_reclamacao_id, usuario_id: @rap.usuario_id }
    end

    assert_redirected_to rap_path(assigns(:rap))
  end

  test "should show rap" do
    get :show, id: @rap
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rap
    assert_response :success
  end

  test "should update rap" do
    patch :update, id: @rap, rap: { analise_critica: @rap.analise_critica, area_responsavel_id: @rap.area_responsavel_id, area_solicitante_id: @rap.area_solicitante_id, consequencia: @rap.consequencia, desc_acao: @rap.desc_acao, desc_nc: @rap.desc_nc, dt_encerramento: @rap.dt_encerramento, dt_prevista: @rap.dt_prevista, justificativa: @rap.justificativa, nova_dt: @rap.nova_dt, solicitante: @rap.solicitante, status: @rap.status, status_acao: @rap.status_acao, tipo_nc_id: @rap.tipo_nc_id, tipo_reclamacao_id: @rap.tipo_reclamacao_id, usuario_id: @rap.usuario_id }
    assert_redirected_to rap_path(assigns(:rap))
  end

  test "should destroy rap" do
    assert_difference('Rap.count', -1) do
      delete :destroy, id: @rap
    end

    assert_redirected_to raps_path
  end
end
