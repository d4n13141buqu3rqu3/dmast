require 'test_helper'

class TipoAtaControllerTest < ActionController::TestCase
  setup do
    @tipo_atum = tipo_ata(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipo_atas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipo_atum" do
    assert_difference('TipoAta.count') do
      post :create, tipo_atum: {  }
    end

    assert_redirected_to tipo_atum_path(assigns(:tipo_atum))
  end

  test "should show tipo_atum" do
    get :show, id: @tipo_atum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipo_atum
    assert_response :success
  end

  test "should update tipo_atum" do
    patch :update, id: @tipo_atum, tipo_atum: {  }
    assert_redirected_to tipo_atum_path(assigns(:tipo_atum))
  end

  test "should destroy tipo_atum" do
    assert_difference('TipoAta.count', -1) do
      delete :destroy, id: @tipo_atum
    end

    assert_redirected_to tipo_ata_index_path
  end
end
