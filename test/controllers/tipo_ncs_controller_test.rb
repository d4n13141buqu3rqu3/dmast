require 'test_helper'

class TipoNcsControllerTest < ActionController::TestCase
  setup do
    @tipo_nc = tipo_ncs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipo_ncs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipo_nc" do
    assert_difference('TipoNc.count') do
      post :create, tipo_nc: { descricao: @tipo_nc.descricao, sigla: @tipo_nc.sigla }
    end

    assert_redirected_to tipo_nc_path(assigns(:tipo_nc))
  end

  test "should show tipo_nc" do
    get :show, id: @tipo_nc
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipo_nc
    assert_response :success
  end

  test "should update tipo_nc" do
    patch :update, id: @tipo_nc, tipo_nc: { descricao: @tipo_nc.descricao, sigla: @tipo_nc.sigla }
    assert_redirected_to tipo_nc_path(assigns(:tipo_nc))
  end

  test "should destroy tipo_nc" do
    assert_difference('TipoNc.count', -1) do
      delete :destroy, id: @tipo_nc
    end

    assert_redirected_to tipo_ncs_path
  end
end
