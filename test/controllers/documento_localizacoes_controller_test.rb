require 'test_helper'

class DocumentoLocalizacoesControllerTest < ActionController::TestCase
  setup do
    @documento_localizacao = documento_localizacoes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:documento_localizacoes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create documento_localizacao" do
    assert_difference('DocumentoLocalizacao.count') do
      post :create, documento_localizacao: { area: @documento_localizacao.area, documento_id: @documento_localizacao.documento_id, localizacao: @documento_localizacao.localizacao, numero: @documento_localizacao.numero, pasta: @documento_localizacao.pasta }
    end

    assert_redirected_to documento_localizacao_path(assigns(:documento_localizacao))
  end

  test "should show documento_localizacao" do
    get :show, id: @documento_localizacao
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @documento_localizacao
    assert_response :success
  end

  test "should update documento_localizacao" do
    patch :update, id: @documento_localizacao, documento_localizacao: { area: @documento_localizacao.area, documento_id: @documento_localizacao.documento_id, localizacao: @documento_localizacao.localizacao, numero: @documento_localizacao.numero, pasta: @documento_localizacao.pasta }
    assert_redirected_to documento_localizacao_path(assigns(:documento_localizacao))
  end

  test "should destroy documento_localizacao" do
    assert_difference('DocumentoLocalizacao.count', -1) do
      delete :destroy, id: @documento_localizacao
    end

    assert_redirected_to documento_localizacoes_path
  end
end
