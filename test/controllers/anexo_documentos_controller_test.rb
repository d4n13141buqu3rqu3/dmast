require 'test_helper'

class AnexoDocumentosControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get create" do
    get :create
    assert_response :success
  end

  test "should get anexo_params" do
    get :anexo_params
    assert_response :success
  end

end
