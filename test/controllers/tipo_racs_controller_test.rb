require 'test_helper'

class TipoRacsControllerTest < ActionController::TestCase
  setup do
    @tipo_rac = tipo_racs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipo_racs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipo_rac" do
    assert_difference('TipoRac.count') do
      post :create, tipo_rac: { descricao: @tipo_rac.descricao }
    end

    assert_redirected_to tipo_rac_path(assigns(:tipo_rac))
  end

  test "should show tipo_rac" do
    get :show, id: @tipo_rac
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipo_rac
    assert_response :success
  end

  test "should update tipo_rac" do
    patch :update, id: @tipo_rac, tipo_rac: { descricao: @tipo_rac.descricao }
    assert_redirected_to tipo_rac_path(assigns(:tipo_rac))
  end

  test "should destroy tipo_rac" do
    assert_difference('TipoRac.count', -1) do
      delete :destroy, id: @tipo_rac
    end

    assert_redirected_to tipo_racs_path
  end
end
