require 'test_helper'

class ConsensadoresControllerTest < ActionController::TestCase
  setup do
    @consensador = consensadores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:consensadores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create consensador" do
    assert_difference('Consensador.count') do
      post :create, consensador: { email: @consensador.email }
    end

    assert_redirected_to consensador_path(assigns(:consensador))
  end

  test "should show consensador" do
    get :show, id: @consensador
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @consensador
    assert_response :success
  end

  test "should update consensador" do
    patch :update, id: @consensador, consensador: { email: @consensador.email }
    assert_redirected_to consensador_path(assigns(:consensador))
  end

  test "should destroy consensador" do
    assert_difference('Consensador.count', -1) do
      delete :destroy, id: @consensador
    end

    assert_redirected_to consensadores_path
  end
end
