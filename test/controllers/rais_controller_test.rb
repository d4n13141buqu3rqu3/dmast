require 'test_helper'

class RaisControllerTest < ActionController::TestCase
  setup do
    @rai = rais(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rais)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rai" do
    assert_difference('Rai.count') do
      post :create, rai: { auditoria_id: @rai.auditoria_id, g_nconform: @rai.g_nconform, objetivos: @rai.objetivos, obs: @rai.obs, resultado: @rai.resultado, usuario_id: @rai.usuario_id }
    end

    assert_redirected_to rai_path(assigns(:rai))
  end

  test "should show rai" do
    get :show, id: @rai
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @rai
    assert_response :success
  end

  test "should update rai" do
    patch :update, id: @rai, rai: { auditoria_id: @rai.auditoria_id, g_nconform: @rai.g_nconform, objetivos: @rai.objetivos, obs: @rai.obs, resultado: @rai.resultado, usuario_id: @rai.usuario_id }
    assert_redirected_to rai_path(assigns(:rai))
  end

  test "should destroy rai" do
    assert_difference('Rai.count', -1) do
      delete :destroy, id: @rai
    end

    assert_redirected_to rais_path
  end
end
