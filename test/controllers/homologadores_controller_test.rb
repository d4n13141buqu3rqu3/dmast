require 'test_helper'

class HomologadoresControllerTest < ActionController::TestCase
  setup do
    @homologador = homologadores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:homologadores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create homologador" do
    assert_difference('Homologador.count') do
      post :create, homologador: { email: @homologador.email }
    end

    assert_redirected_to homologador_path(assigns(:homologador))
  end

  test "should show homologador" do
    get :show, id: @homologador
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @homologador
    assert_response :success
  end

  test "should update homologador" do
    patch :update, id: @homologador, homologador: { email: @homologador.email }
    assert_redirected_to homologador_path(assigns(:homologador))
  end

  test "should destroy homologador" do
    assert_difference('Homologador.count', -1) do
      delete :destroy, id: @homologador
    end

    assert_redirected_to homologadores_path
  end
end
