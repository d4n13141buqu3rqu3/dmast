require 'test_helper'

class TipoReclamacoesControllerTest < ActionController::TestCase
  setup do
    @tipo_reclamacao = tipo_reclamacaos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipo_reclamacaos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipo_reclamacao" do
    assert_difference('TipoReclamacao.count') do
      post :create, tipo_reclamacao: { descricao: @tipo_reclamacao.descricao }
    end

    assert_redirected_to tipo_reclamacao_path(assigns(:tipo_reclamacao))
  end

  test "should show tipo_reclamacao" do
    get :show, id: @tipo_reclamacao
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipo_reclamacao
    assert_response :success
  end

  test "should update tipo_reclamacao" do
    patch :update, id: @tipo_reclamacao, tipo_reclamacao: { descricao: @tipo_reclamacao.descricao }
    assert_redirected_to tipo_reclamacao_path(assigns(:tipo_reclamacao))
  end

  test "should destroy tipo_reclamacao" do
    assert_difference('TipoReclamacao.count', -1) do
      delete :destroy, id: @tipo_reclamacao
    end

    assert_redirected_to tipo_reclamacaos_path
  end
end
