# README #

Documento de base para quem adotar o projeto


### O quẽ eu preciso para começar? ###

* git clone ....
* rake db:create
* rake db:migrate ou rake db:setup
* rake db:seed para popular a base de dados com alguns registros importantes

### Dicas ###

* Escreva testes!
* Revise o código!
* Tome café!

### Com quem eu devo pedir suporte? ###

* daniel.alb.querque@gmail.com.br
* daniel.albuquerque@amcel.com.br



### APT-GET ###
* sudo apt-get install wkhtmltopdf

