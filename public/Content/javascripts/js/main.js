$(document).ready(function(){

  var pastas = $('#documento_localizacao_pasta_id').html();
  var numeros = $('#documento_localizacao_numero_id').html();
  var localizacoes = $('#documento_localizacao_localizacao_id').html();

  $('.pasta').hide();
  $('.numero').hide();
  $('.localizacao').hide();

  $('#documento_localizacao_area_id').change(function(){
    $('.pasta').hide();
    $('.numero').hide();
    $('.localizacao').hide();
    var area = $('#documento_localizacao_area_id :selected').text();
    var options = "<option> - Selecione - </option>";
    options = options + $(pastas).filter("optgroup[label='"+area+"']").html();
    if(options.length > 32 ){
      $('#documento_localizacao_pasta_id').html(options);
      $('.pasta').show();
    }else{
      $('#documento_localizacao_pasta_id').empty();
      $('.pasta').hide();
    }
  });

  $('#documento_localizacao_pasta_id').change(function(){
    $('.numero').hide();
    $('.localizacao').hide();
    var pasta = $('#documento_localizacao_pasta_id :selected').text();
    var options = "<option> - Selecione - </option>";
    options = options + $(numeros).filter("optgroup[label='"+pasta+"']").html();
    if(options.length > 32){
      $('#documento_localizacao_numero_id').html(options);
      $('.numero').show();
    }else{
      $('#documento_localizacao_numero_id').empty();
      $('.numero').hide();
    }
  });

  $('#documento_localizacao_numero_id').change(function(){
    $('.localizacao').hide();
    var numero = $('#documento_localizacao_numero_id :selected').text();
    var options = "<option> - Selecione - </option>";
    options = options + $(localizacoes).filter("optgroup[label='"+numero+"']").html();
    if(options.length > 32){
      $('#documento_localizacao_localizacao_id').html(options);
      $('.localizacao').show();
    }else{
      $('#documento_localizacao_localizacao_id').empty();
      $('.localizacao').hide();
    }

  });


  $('#btn-followup').click(function(){
      $(window.document.location).attr('href','/follow-up-auditoria-' + $('#followup_select').val());
  });

  $('input#id_search').quicksearch('table#table_example tbody tr');

  $('#btn-plano-select').click(function(){
      $(window.document.location).attr('href','/auditorias,plano,' + $('#plano_select').val());
  });

  $('#btn-rac-select').click(function(){
      $(window.document.location).attr('href','/racs,visualizar,' + $('#rac_select').val());
  });

  $('#btn-rap-select').click(function(){
      $(window.document.location).attr('href','/raps,visualizar,' + $('#rap_select').val());
  });

    $('#btn-rap-select').click(function(){
        $(window.document.location).attr('href','/raps,visualizar,' + $('#rap_select').val());
    });
  
  $('#btn-add-followup').click(function(){
      $(window.document.location).attr('href','/add-destinatarios?id='+ $('#auditoria_id').val() + "&email=" + $('#txt_email_followup').val());
  });

  $('#loader').hide();


  $('#menu_aud').on('click', function(){
      $('#content_modal').empty();
      $('#plano_modal').modal();

      $.ajax({
        url: '/ajax_planos',
        data: {
          format: 'html'
        },
        error: function() {
          alert('Falha ao carregar os dados')
        },
        success: function(data) {
          $('#content_modal').append(data);
          $('#loading_planos').hide();
          $('#btn-plano-select').show();
        },
        type: 'GET'
      });
  });

  $('#tab_ajuda').on('click', function(){
      $.ajax({
        url: '/ajax_ajuda',
        data: {
          format: 'html'
        },
        error: function(data, status) {
          alert('Falha ao carregar dados');
        },
        success: function(data) {
          $('#content_helper').empty();
          $('#content_helper').append(data);
        },
        type: 'GET'
      });
  });

});

  



