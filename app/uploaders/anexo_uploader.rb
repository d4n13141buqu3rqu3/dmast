# encoding: utf-8

class AnexoUploader < CarrierWave::Uploader::Base

  storage :file

  def store_dir
    "anexos/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_white_list
    %w(jpg jpeg gif png doc docx pdf xls xlsx)
  end

end
