module ApplicationHelper
  def formata_dt(date)
    date.strftime("%d/%m/%Y")
  end

  def formata_dt_hr(date)
    date.strftime("%d/%m/%Y - %H:%M")
  end

  def amcel_log(value)
    logger.debug " "
    logger.debug "#############################################################"
    logger.debug "#############################################################"
    logger.debug " "
    logger.debug value
    logger.debug " "
    logger.debug "#############################################################"
    logger.debug "#############################################################"
  end
end
