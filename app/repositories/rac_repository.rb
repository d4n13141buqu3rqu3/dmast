class RacRepository
  def self.save(data)
    @rac = Rac.new(data)
    ultima_sequencia = Rac.where(ano: @rac.dt_criacao.year).maximum(:sequencia)

    sequencia = 1

    if ultima_sequencia.present?
      sequencia = ultima_sequencia
      sequencia += 1
    end

    @rac.ano       = @rac.dt_criacao.year
    @rac.sequencia = sequencia
    @rac.save!

    if @rac.notifica_responsavel #Notifica o responsável pelo RAC
      dados = {
          plano_auditoria:        @rac.auditoria.planoauditoria.nome,
          auditoria:              @rac.auditoria.numero,
          dpto:                   @rac.auditoria.area.descricao,
          solicitante:            @rac.solicitante,
          tp_n_conformidade:      @rac.tipo_nc.descricao,
          desc_n_conformdiade:    @rac.desc_nc
      }


      email = Funcionario.find_by_nome(@rac.responsavel).email

      Thread.new { NotificacaoMailer.rac(email, dados).deliver }

    end
    return @rac
  end
end