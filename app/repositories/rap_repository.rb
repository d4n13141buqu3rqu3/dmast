class RapRepository
  def self.save(data)
    @rap = Rap.new(data)
    ultima_sequencia = Rap.where(ano: @rap.dt_criacao.year).maximum(:sequencia)

    sequencia = 1

    if ultima_sequencia.present?
      sequencia = ultima_sequencia
      sequencia += 1
    end

    @rap.ano       = @rap.dt_criacao.year
    @rap.sequencia = sequencia
    @rap.save!
    return @rap
  end
end