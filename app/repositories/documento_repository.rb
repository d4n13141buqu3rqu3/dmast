class DocumentoRepository

	def self.reenviar_workflow(params)
		@documento = Documento.find params[:id]

		if params[:email].present?
			emails = params[:email]
			dados = []
			dados << User.current.name
			if @documento.status == 'Enviado para consenso'
				dados << "#{params[:url]}/consenso-documento-#{params[:id]}"
				Thread.new { emails.each do |e| WorkflowMailer.msg(e, dados).deliver  if e.present? end }
			else
				dados << "#{params[:url]}/homologar-documento-#{params[:id]}"
				Thread.new {  emails.each do |email|  WorkflowMailer.enviar_homologacao(email, dados).deliver if email.present? end }
			end

		end
	end

	def self.salva(params)
		user = User.current
		if params[:documento_id].present?
			Documento.update(params)
		else
			@doc = Documento.create(params)
			DocumentoHistorico.create documento: @doc, historico: "Documento criado por #{User.current.name}"

			#Verifica se o documento criado é uma nova versao de outro doc, caso sim: enviar a versao antiga para lixeira
			if @doc.doc_pai_id.present?
				doc_anterior = Documento.find @doc.doc_pai_id
				if doc_anterior.present?
					#Marca o documento como excluído
					doc_anterior.update excluido: true, motivo_exclusao: "Nova versao criada", deleted_at: Time.now

					#Salva o log
					historico = "Documento enviado para a lixeira pelo usuário #{User.current.name} - motivo: #{doc_anterior.motivo_exclusao}"
					DocumentoHistorico.create documento: doc_anterior, historico: historico

					#remove o documento da lista de atividades
					atividade = Atividade.find_by_descricao doc_anterior.sigla
					atividade.destroy if atividade.present?
				end
			end
		end
		return @doc
	end

	def self.enviar_para_consenso(params)
		doc 		  = Documento.find params[:id]
		grp           = GrpConsensador.find doc.grp_consenso_id
		consensadores = grp.funcionarios
		dados         = []

		dados << User.current.name
		dados << "#{params[:url]}/consenso-documento-#{params[:id]}"

		Thread.new {
			consensadores.each do |c|
				WorkflowMailer.msg(c.email, dados).deliver if c.email.present?
			end
		}

		consensadores.each do |c|
			Consensamento.create(documento_id: params[:id], status: "Resposta pendente", email: c.email) if c.email.present?
			Notificacao.create descricao: "Documento esperando consenso", url: "#{params[:url]}/consenso-documento-#{params[:id]}", user: User.current, notiemail: c.email, visto: false
		end

		#doc = Documento.find params[:id]
		doc.update dt_envio_consenso: Time.now, status: "Enviado para consenso"
		DocumentoHistorico.create documento: doc, historico: "Enviado para consenso por #{User.current.name}"
		return doc
	end

	def self.consenso(params)
		consensado = params[:consensado].present? ? params[:consensado] : true
		status = params[:status].present? ? params[:status] : "Consensado"

		resp = Consensamento.find params[:id]
		resp.update consensado: consensado, motivo_nao_consensamento: params[:motivo], status: status

		if resp.consensado == false
			documento = Documento.update resp.documento_id, status: 'Em elaboração'
			DocumentoHistorico.create documento_id: resp.documento_id, historico: "Consenso negado por #{resp.email} - Motivo: #{resp.motivo_nao_consensamento}"
			dados = []
			dados << resp.email
			dados << "#{params[:url]}/atualizar-documento-#{resp.documento_id}"
			dados << resp.motivo_nao_consensamento
			Thread.new { WorkflowMailer.consenso_negado(resp.documento.user.email, dados).deliver if resp.documento.user.email.present? }
			Notificacao.create descricao: "Consenso negado", url: "#{params[:url]}/atualizar-documento-#{params[:id]}", user: User.current, notiemail: documento.user.email, visto: false
		else
			consensos          = Consensamento.where("documento_id = ? and status != ?", resp.documento_id, "Não consensado")
			consenso_aprovados = Consensamento.where("documento_id = ? and consensado = ? and status != ?", resp.documento_id, true, "Não consensado")
			if consensos.count == consenso_aprovados.count #Envia para homologação caso tenha sido consensado
				documento = Documento.find resp.documento_id
				documento.update dt_consenso: Time.now, dt_envio_homologacao: Time.now, status: "Enviado para homologação"
				DocumentoHistorico.create documento_id: resp.documento_id, historico:  "Documento consensado"
				DocumentoHistorico.create documento_id: resp.documento_id, historico: "Documento enviado para homologação."
				Notificacao.create descricao: "Documento consensado e enviado para homologação.", url: "#{params[:url]}/atualizar-documento-#{params[:id]}", user: User.current, notiemail: documento.user.email
				Thread.new { WorkflowMailer.consenso_aprovado(resp.documento.user.email, resp.documento.titulo).deliver if resp.documento.user.email.present? }
				self.inicia_workflow_homologacao({documento_id: documento.id,
																						 email: documento.user.email,
																						 grp_homologador_id: documento.grp_homologador_id,
																						 url: params[:url]
																				 })
			end
		end
		return resp
	end

	def self.homologacao(params)
		homologado = params[:homologado].present? ? params[:homologado] : true
		status     = params[:status].present? ? params[:status] : "Homologado"
		resp       = Homologacao.find params[:id]

		resp.update homologado: homologado, motivo_nao_homolog: params[:motivo], status: status

		if resp.homologado == false

			documento = Documento.find resp.documento_id
			documento.update(status: "Em elaboração")
			
			DocumentoHistorico.create documento: documento, historico: "Homologação negada por #{resp.email}"

			Notificacao.create do |n|
				n.descricao = "Documento não foi homologado"
				n.url = "#{params[:url]}/atualizar-documento-#{params[:id]}"
				n.user = params[:user]
				n.notiemail = documento.user.email
				n.visto = false
			end

			dados = []
			dados << resp.email
			dados << "#{params[:url]}/atualizar-documento-#{resp.documento_id}"
			dados << resp.motivo_nao_homolog

			Thread.new { WorkflowMailer.homologacao_negada(resp.documento.user.email, dados).deliver if resp.documento.user.email.present? }
		else
			homologacoes = Homologacao.where("documento_id = ? and status != ?", resp.documento_id, "Não homologado")
			homologacoes_aprovadas = Homologacao.where("documento_id = ? and homologado = ? and status != ?", resp.documento_id, true, "Não consensado")
			if homologacoes_aprovadas.count == homologacoes.count
				documento = Documento.update resp.documento_id, dt_homologacao: Time.now, status: "Documento oficial"
				DocumentoHistorico.create documento_id: documento.id, historico: "Documento homologado"
				Atividade.create(descricao: documento.sigla, id: Atividade.last.id + 1)
				Notificacao.create(descricao: "Documento homologado", url: "#{params[:url]}/atualizar-documento-#{params[:id]}", user: User.current, notiemail: documento.user.email, visto: false)
				Thread.new { WorkflowMailer.homologacao_aprovada(resp.documento.user.email, resp.documento.titulo).deliver  if resp.documento.user.email.present?}
				#Thread.new { WorkflowMailer.homologacao_notificao_all(resp.documento.user.email, resp.documento.titulo).deliver } if Rails.env.production?
			end
		end
		resp
	end

	def self.excluir(params)
		doc = Documento.find(params[:id])
		if doc.present?
			doc.update excluido: true, motivo_exclusao: params[:motivo], deleted_at: Time.now
			historico = "Documento enviado para a lixeira pelo usuário #{User.current.name} - motivo: #{doc.motivo_exclusao}"
			DocumentoHistorico.create documento: doc, historico: historico
			atividade = Atividade.find_by_descricao doc.sigla
			atividade.destroy if atividade.present?
		end
	end

	def self.restaurar(params)
		doc = Documento.find(params[:id])
		if doc.present?
			doc.update excluido: false, motivo_exclusao: nil, deleted_at: nil
			historico = "Documento restaurado por #{User.current.name} - motivo: #{params[:motivo]}"
			DocumentoHistorico.create documento: doc, historico: historico
			Atividade.create descricao: doc.sigla if doc.status == "Documento oficial"
		end
	end

	private

	def self.inicia_workflow_homologacao(params)
		#Busca os homologadores desse documento
		grp           = GrpHomologador.find params[:grp_homologador_id]
		homologadores = grp.funcionarios

		#Notificação para cada homologador
		homologadores.each do |h|
			Notificacao.create descricao: "Documento esperando homologaçao.", url: "#{params[:url]}/homologar-documento-#{params[:documento_id]}", user: User.current, notiemail: params[:email], visto: false
		end

		#Dispara o email para os homolgadores
		dados = []
		dados << params[:email]
		dados << "#{params[:url]}/homologar-documento-#{params[:documento_id]}"
		homologadores.each do |h|
			Thread.new { WorkflowMailer.enviar_homologacao(h.email, dados).deliver if h.email.present? }
			Homologacao.create(documento_id: params[:documento_id], status: "Resposta pendente", email: h.email) if h.email.present?
		end
	end

	def self.inicia_workflow_consenso(params)
	end


end