class DemaisRacRepository
  def self.save(data)
    @rac = DemaisRac.new(data)
    ultima_sequencia = DemaisRac.where(ano: @rac.dt_criacao.year).maximum(:sequencia)

    sequencia = 1

    if ultima_sequencia.present?
      sequencia = ultima_sequencia
      sequencia += 1
    end

    @rac.ano       = @rac.dt_criacao.year
    @rac.sequencia = sequencia
    @rac.save!
    return @rac
  end
end