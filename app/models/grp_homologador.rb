# == Schema Information
#
# Table name: grp_homologadores
#
#  id        :integer          not null, primary key
#  descricao :string(50)
#

class GrpHomologador < ActiveRecord::Base
  has_and_belongs_to_many :funcionarios, join_table: :grphomologadores_funcionarios, class_name: 'Funcionario'
  has_many :documentos
end
