# == Schema Information
#
# Table name: grupos
#
#  id         :integer          not null, primary key
#  descricao  :string(50)       not null
#  created_at :datetime
#  updated_at :datetime
#

class Grupo < ActiveRecord::Base
  has_many :funcionarios
end
