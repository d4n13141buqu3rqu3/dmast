# == Schema Information
#
# Table name: raps
#
#  id                  :integer          not null, primary key
#  status              :string(255)
#  tipo_reclamacao_id  :integer
#  area_responsavel_id :integer
#  area_solicitante_id :integer
#  solicitante         :string(60)
#  tipo_nc_id          :integer
#  desc_nc             :text
#  status_acao         :string(25)
#  desc_acao           :text
#  dt_prevista         :datetime
#  nova_dt             :datetime
#  justificativa       :text
#  analise_critica     :text
#  dt_encerramento     :datetime
#  created_at          :datetime
#  updated_at          :datetime
#  inf                 :text
#  sequencia           :integer
#  ano                 :integer
#  dt_criacao          :datetime
#  user_id             :integer
#

class Rap < ActiveRecord::Base

  before_validation :set_status, :verifica_adiamento

  belongs_to :tipo_reclamacao
  belongs_to :area_responsavel, class_name: 'Area', foreign_key: 'area_responsavel_id'
  belongs_to :area_solicitante, class_name: 'Area', foreign_key: 'area_solicitante_id'
  belongs_to :tipo_nc
  belongs_to :funcionario
  belongs_to :auditoria
  belongs_to :area
  belongs_to :user
  has_many :rap_historicos, dependent: :destroy
  has_and_belongs_to_many :funcionarios, join_table: :raps_participantes, dependent: :destroy, class_name: 'Usuario'
  has_many :rap_followups

  def set_status
    self.status = 'Aguardando retorno' if self.dt_prevista.present?
    self.status = 'Encerrado' if self.dt_encerramento.present? 
  end

  def verifica_adiamento
    if self.nova_dt.present?
      dt_anterior = self.dt_prevista
      self.dt_prevista = self.nova_dt
      #salva o log de alteração
      hist = RapHistorico.new
      hist.rap = self
      hist.dtAntiga = dt_anterior
      hist.dtNova = self.nova_dt
      hist.justificativa = self.justificativa
      hist.historico = "Ação preventiva adiada."
      hist.save!

      #limpa os campos
      self.nova_dt = nil
      self.justificativa = nil
    end
  end
end
