# == Schema Information
#
# Table name: auditorias
#
#  id                :integer          not null, primary key
#  planoauditoria_id :integer
#  status            :string(255)
#  area_id           :integer
#  data_prevista     :datetime
#  auditor_lider_id  :integer
#  duracao_prevista  :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  hora_prevista     :string(255)
#  legislacao        :text
#  numero            :string(255)
#  criado_por        :integer
#

class Auditoria < ActiveRecord::Base
  belongs_to :planoauditoria
  belongs_to :area
  has_many :rais, dependent: :destroy
  has_many :racs, dependent: :destroy
  has_many :auditoria_atividades
  belongs_to :auditor, class_name: 'Funcionario', foreign_key: 'auditor_lider_id'
  belongs_to :criado_por, class_name: 'User', foreign_key: 'criado_por'
  has_and_belongs_to_many :participantes, join_table: :auditorias_funcionarios, dependent: :destroy, class_name: 'Funcionario'
  has_and_belongs_to_many :atividades, join_table: :auditoria_atividades, dependent: :destroy, class_name: 'Atividade'


  scope :pendentes_principais, -> {
     includes(:planoauditoria)
    .includes(:auditor)
    .where("status != ?", "Finalizado")
    .order("data_prevista desc")
    .take(10)
  }

end
