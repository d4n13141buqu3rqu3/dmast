# == Schema Information
#
# Table name: auditoria_atividades
#
#  id           :integer          not null, primary key
#  auditoria_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#  atividade_id :integer
#

class AuditoriaAtividade < ActiveRecord::Base
  belongs_to :auditoria
  belongs_to :atividade
end
