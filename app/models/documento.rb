# == Schema Information
#
# Table name: documentos
#
#  id                   :integer          not null, primary key
#  titulo               :string(80)
#  tipoDoc_id           :integer
#  status               :string(30)
#  texto_doc            :text
#  dias_consenso        :integer
#  dias_homologar       :integer
#  dt_envio_consenso    :datetime
#  dt_envio_homologacao :datetime
#  sigla                :string(30)
#  versao               :string(50)
#  versao_anterior      :string(50)
#  id_doc_anterior      :integer
#  link_versao_anterior :string(200)
#  created_at           :datetime
#  dt_consenso          :datetime
#  dt_homologacao       :datetime
#  user_id              :integer
#  elaborador           :string(50)       not null
#  excluido             :boolean          default(FALSE), not null
#  motivo_exclusao      :string(50)
#  deleted_at           :datetime
#  grp_consenso_id      :integer
#  grp_homologador_id   :integer
#  doc_pai_id           :integer
#

class Documento < ActiveRecord::Base
  belongs_to :tipoDoc
  belongs_to :user
  belongs_to :grp_consenso
  belongs_to :grp_homologador
  has_many :documento_historicos, dependent: :destroy
  has_many :anexo_docs
  has_many :documento_localizacoes
  has_and_belongs_to_many :areas, join_table: :documentos_areas, dependent: :destroy, class_name: 'Area'
  

  scope :lixo, -> {
    where(excluido: true)
    .order("created_at desc")
  }

  def self.documentos_ativos(status = "Documento oficial")
     where(excluido: false)
    .where(status: status)
    .includes(:tipoDoc)
    .order("created_at desc")
  end
end
