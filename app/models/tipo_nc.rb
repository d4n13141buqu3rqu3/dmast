# == Schema Information
#
# Table name: tipo_ncs
#
#  id         :integer          not null, primary key
#  sigla      :string(10)
#  descricao  :string(80)
#  created_at :datetime
#  updated_at :datetime
#

class TipoNc < ActiveRecord::Base
  has_many :racs, dependent: :destroy
end
