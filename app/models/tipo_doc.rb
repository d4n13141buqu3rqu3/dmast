# == Schema Information
#
# Table name: tipo_docs
#
#  id         :integer          not null, primary key
#  descricao  :string(50)
#  created_at :datetime
#  updated_at :datetime
#

class TipoDoc < ActiveRecord::Base
  has_many :documentos
end
