# == Schema Information
#
# Table name: notificacoes
#
#  id         :integer          not null, primary key
#  descricao  :string(255)
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#  url        :string(255)
#  visto      :boolean
#  notiemail  :string(100)      not null
#

class Notificacao < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :descricao, :user, :notiemail



end
