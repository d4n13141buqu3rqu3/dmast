# == Schema Information
#
# Table name: documento_historicos
#
#  id           :integer          not null, primary key
#  documento_id :integer
#  historico    :text
#  created_at   :datetime
#  updated_at   :datetime
#

class DocumentoHistorico < ActiveRecord::Base
  belongs_to :documento


end
