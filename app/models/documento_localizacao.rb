# == Schema Information
#
# Table name: documento_localizacoes
#
#  id           :integer          not null, primary key
#  documento_id :integer
#  area         :string(255)
#  pasta        :string(255)
#  numero       :string(255)
#  localizacao  :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#

class DocumentoLocalizacao < ActiveRecord::Base
  belongs_to :documento
  belongs_to :area
  belongs_to :pasta
  belongs_to :numero
  belongs_to :localizacao
end
