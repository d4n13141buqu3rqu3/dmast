# == Schema Information
#
# Table name: rac_historicos
#
#  id                :integer          not null, primary key
#  rac_id            :integer
#  historico         :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  NovaDt            :datetime
#  DtAnterior        :datetime
#  justificativa     :text
#  NovoResponsavel   :string(255)
#  AntigoResponsavel :string(255)
#  user_id           :integer
#

class RacHistorico < ActiveRecord::Base
  belongs_to :rac
  belongs_to :user
end
