# == Schema Information
#
# Table name: consensamentos
#
#  id                       :integer          not null, primary key
#  documento_id             :integer
#  consensado               :boolean
#  motivo_nao_consensamento :text
#  created_at               :datetime
#  updated_at               :datetime
#  status                   :string(255)
#  email                    :string(255)
#

class Consensamento < ActiveRecord::Base
  belongs_to :documento
end
