# == Schema Information
#
# Table name: topicos
#
#  id         :integer          not null, primary key
#  descricao  :string(255)
#  encerrado  :boolean
#  reuniao_id :integer
#  created_at :datetime
#  updated_at :datetime
#

class Topico < ActiveRecord::Base
  belongs_to :reuniao
end
