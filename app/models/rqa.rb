# == Schema Information
#
# Table name: rqas
#
#  id          :integer          not null, primary key
#  descricao   :text             not null
#  proposta    :text
#  responsavel :string(60)       not null
#  prazo       :string(30)
#  dt          :datetime         not null
#  area_id     :integer          not null
#  created_at  :datetime
#  updated_at  :datetime
#  filial      :string(255)
#  categoria   :string(255)
#  AI          :boolean          default(FALSE)
#  CI          :boolean          default(FALSE)
#  user_id     :integer
#

class Rqa < ActiveRecord::Base

  validates_presence_of :descricao, :responsavel, :proposta, :filial, :dt, :CI, :AI
  validates_associated :area, :user

  belongs_to :area
  belongs_to :user

  #Método responsável por salvar um RQA
  def self.add(params)
    row = create(self.montar(params))
    params[:area]        = row.area.descricao
    params[:email_setor] = row.area.email
    params[:usuario]     = User.current.name
    
    Thread.new {
      NotificacaoMailer.rqa_sesmet(params).deliver
      NotificacaoMailer.rqa_setor(params).deliver  
    }
    
  end

  private
    def self.montar(params)
      {
          descricao: params[:descricao], responsavel: params[:responsavel],
          area_id:   params[:area_id],   proposta:    params[:proposta], filial: params[:filial],
          dt:        params[:data],      CI:          params[:ci],       AI:     params[:ci],
          user:      User.current
      }
    end
end
