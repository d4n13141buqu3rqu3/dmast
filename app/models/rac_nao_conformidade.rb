# == Schema Information
#
# Table name: rac_nao_conformidades
#
#  id         :integer          not null, primary key
#  rac_id     :integer
#  tipo_nc_id :integer
#  descricao  :text
#  created_at :datetime
#  updated_at :datetime
#

class RacNaoConformidade < ActiveRecord::Base
  belongs_to :rac
  belongs_to :tipo_nc
end
