# == Schema Information
#
# Table name: demais_racs
#
#  id                         :integer          not null, primary key
#  status                     :string(25)
#  tipo_reclamacao_id         :integer
#  area_solicitante_id        :integer
#  area_responsavel_id        :integer
#  user_id                    :integer
#  causa_raiz                 :text
#  desc_acao_corretiva        :text
#  dt_acao_corretiva          :datetime
#  nova_dt_prevista           :datetime
#  just_nova_dt               :text
#  responsavel_acao_corretiva :string(80)
#  anal_critica               :text
#  dt_real_encerramento       :datetime
#  created_at                 :datetime
#  updated_at                 :datetime
#  tipo_nc_id                 :integer
#  desc_nc                    :text
#  solicitante                :string(50)
#  ano                        :integer          not null
#  sequencia                  :integer          not null
#  NovoResponsavel            :string(50)
#  dt_criacao                 :datetime         not null
#  status_correcao            :string(30)       default("Pendente"), not null
#

class DemaisRac < ActiveRecord::Base
  belongs_to :area_responsavel, class_name: 'Area', foreign_key: 'area_solicitante_id'
  belongs_to :area_solicitante, class_name: 'Area', foreign_key: 'area_responsavel_id'
  belongs_to :tipo_nc
  belongs_to :tipo_reclamacao
  belongs_to :user
  has_many :demais_rac_historicos


end
