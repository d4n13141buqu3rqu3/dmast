# == Schema Information
#
# Table name: rais
#
#  id           :integer          not null, primary key
#  auditoria_id :integer
#  objetivos    :text
#  resultado    :text
#  obs          :text
#  g_nconform   :boolean
#  created_at   :datetime
#  updated_at   :datetime
#  lider_id     :integer
#  user_id      :integer
#  docs_ref     :text
#

class Rai < ActiveRecord::Base
  belongs_to :auditoria
  belongs_to :user
  belongs_to :auditor_lider, class_name: 'Funcionario', foreign_key: 'lider_id'
  has_and_belongs_to_many :funcionarios, join_table: :rais_participantes, dependent: :destroy, class_name: 'Funcionario'
  has_many :rai_followups
end
