# == Schema Information
#
# Table name: rap_historicos
#
#  id            :integer          not null, primary key
#  rap_id        :integer
#  historico     :string(255)
#  dtAntiga      :datetime
#  dtNova        :datetime
#  justificativa :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class RapHistorico < ActiveRecord::Base
  belongs_to :rap
  belongs_to :user
end
