# == Schema Information
#
# Table name: users
#
#  id               :integer          not null, primary key
#  provider         :string(255)
#  uid              :string(255)
#  name             :string(255)
#  oauth_token      :string(255)
#  photo            :string(255)
#  email            :string(255)
#  oauth_expires_at :datetime
#  created_at       :datetime
#  updated_at       :datetime
#

class User < ActiveRecord::Base
  has_many :rais
  has_many :notificacoes
  has_many :documentos
  has_one :funcionario

	def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
      user.provider 		      = auth.provider
     	user.uid 			          = auth.uid
     	user.name 			        = auth.info.name
    	user.email 			        = auth.info.email
     	user.photo 			        = auth.info.image
     	user.oauth_token 	      = auth.credentials.token
    	user.oauth_expires_at   = Time.at auth.credentials.expires_at
     	user.save!
    end
  end

  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end




end
