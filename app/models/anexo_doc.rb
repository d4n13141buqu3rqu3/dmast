# == Schema Information
#
# Table name: anexo_docs
#
#  id           :integer          not null, primary key
#  anexo        :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  documento_id :integer
#  descricao    :string(255)
#  user_id      :integer
#

class AnexoDoc < ActiveRecord::Base
  mount_uploader :anexo, AnexooUploader

  belongs_to :documento
  belongs_to :user
end
