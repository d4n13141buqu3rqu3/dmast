# == Schema Information
#
# Table name: funcionarios
#
#  id         :integer          not null, primary key
#  chapa      :string(10)       not null
#  nome       :string(80)       not null
#  ativo      :boolean          default(TRUE), not null
#  email      :string(200)
#  filial_id  :integer          not null
#  area_id    :integer
#  grupo_id   :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class Funcionario < ActiveRecord::Base
  belongs_to :filial
  belongs_to :area
  belongs_to :grupo
  has_and_belongs_to_many :auditorias, join_table: :auditorias_funcionarios
  has_and_belongs_to_many :reunioes, join_table: :reunioes_usuarios
  has_and_belongs_to_many :planosauditorias, join_table: :planos_funcionarios
  has_and_belongs_to_many :grp_consensador, join_table: :grpconsensador_funcionarios, class_name: 'GrpConsensador'
  has_and_belongs_to_many :grp_homologador, join_table: :grphomologadores_funcionarios, class_name: 'GrpHomologador'


  scope :ativos, -> {
    where(ativo: true)
    .order("nome asc")
  }

  scope :ativos_com_email, -> {
    where(ativo: true).
    where.not(email: nil).
    order("nome asc")
  }

end
