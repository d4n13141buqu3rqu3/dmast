# == Schema Information
#
# Table name: filiais
#
#  id     :integer          not null, primary key
#  codigo :string(5)        not null
#  nome   :string(25)       not null
#

class Filial < ActiveRecord::Base
  has_many :funcionarios
end
