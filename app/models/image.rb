# == Schema Information
#
# Table name: images
#
#  id                :integer          not null, primary key
#  alt               :string(255)      default("")
#  hint              :string(255)      default("")
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  created_at        :datetime
#  updated_at        :datetime
#

class Image < ActiveRecord::Base
  has_attached_file :file
end
