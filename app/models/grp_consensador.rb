# == Schema Information
#
# Table name: grp_consensadores
#
#  id        :integer          not null, primary key
#  descricao :string(50)
#

class GrpConsensador < ActiveRecord::Base
  has_and_belongs_to_many :funcionarios, join_table: :grpconsensador_funcionarios, class_name: 'Funcionario'
  has_many :documentos
  
end
