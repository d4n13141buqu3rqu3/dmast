# == Schema Information
#
# Table name: planosauditorias
#
#  id          :integer          not null, primary key
#  nome        :string(100)      not null
#  encerrado   :boolean          default(FALSE)
#  prazo       :integer          not null
#  ano         :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  data_inicio :datetime
#  data_fim    :datetime
#

class Planoauditoria < ActiveRecord::Base
	has_and_belongs_to_many :funcionarios, join_table: :planos_funcionarios, dependent: :destroy
	has_many :auditorias, dependent: :destroy
end
