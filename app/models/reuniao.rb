# == Schema Information
#
# Table name: reunioes
#
#  id            :integer          not null, primary key
#  assunto       :string(150)
#  data_reuniao  :datetime         not null
#  area_id       :integer          not null
#  TipoAta_id    :integer          not null
#  descricao     :text
#  created_at    :datetime
#  updated_at    :datetime
#  status        :string(255)
#  local_reuniao :string(100)      not null
#

class Reuniao < ActiveRecord::Base
  belongs_to :area
  belongs_to :TipoAta
  belongs_to :local
  has_many :topicos, dependent: :destroy
  has_many :anexo_reuniaos, dependent: :destroy

  has_and_belongs_to_many :funcionarios, join_table: :reunioes_funcionarios,  dependent: :destroy
end
