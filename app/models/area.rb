# == Schema Information
#
# Table name: areas
#
#  id         :integer          not null, primary key
#  descricao  :string(80)       not null
#  created_at :datetime
#  updated_at :datetime
#  email      :string(100)
#

class Area < ActiveRecord::Base
	has_many :reunioes, dependent: :destroy
	has_many :auditorias
	has_many :rqas
	has_many :demais_racs
	has_many :pastas
end
