# == Schema Information
#
# Table name: homologacoes
#
#  id                 :integer          not null, primary key
#  documento_id       :integer
#  homologado         :boolean
#  motivo_nao_homolog :text
#  created_at         :datetime
#  updated_at         :datetime
#  status             :string(255)
#  email              :string(255)
#

class Homologacao < ActiveRecord::Base
  belongs_to :documento
end
