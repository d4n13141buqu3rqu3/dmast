# == Schema Information
#
# Table name: demais_rac_historicos
#
#  id                :integer          not null, primary key
#  demais_rac_id     :integer          not null
#  historico         :string(100)      not null
#  NovaDt            :datetime         not null
#  DtAnterior        :datetime         not null
#  justificativa     :text             not null
#  NovoResponsavel   :string(50)       not null
#  AntigoResponsavel :string(50)       not null
#  user_id           :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#

class DemaisRacHistorico < ActiveRecord::Base
  belongs_to :demais_rac
  belongs_to :user
end
