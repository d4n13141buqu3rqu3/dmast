# == Schema Information
#
# Table name: anexo_reunioes
#
#  id         :integer          not null, primary key
#  reuniao_id :integer
#  descricao  :string(255)
#  anexo      :string(255)
#  created_at :datetime
#  updated_at :datetime
#  downloads  :integer          default(0)
#  user_id    :integer
#

class AnexoReuniao < ActiveRecord::Base
  mount_uploader :anexo, AnexoUploader

  belongs_to :reuniao
  belongs_to :user

  class Array
    def / len
      inject([]) do |ary, x|
        ary << [] if [*ary.last].nitems % len == 0
        ary.last << x
        ary
      end
    end
  end
end
