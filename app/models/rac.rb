# == Schema Information
#
# Table name: racs
#
#  id                    :integer          not null, primary key
#  auditoria_id          :integer
#  status                :string(20)
#  responsavel           :string(60)
#  analise_causa         :text
#  status_correcao       :string(25)
#  desc_correacao        :text
#  dt_prevista_correacao :datetime
#  nova_dt_prevista      :datetime
#  justificativa         :text
#  responsavel_correcao  :string(100)
#  analise_eficacia      :text
#  dt_real_encerramento  :datetime
#  dt_real_execucao      :datetime
#  created_at            :datetime
#  updated_at            :datetime
#  tipo_nc_id            :integer
#  desc_nc               :text
#  NovoResponsavel       :string(255)
#  dt_criacao            :datetime         not null
#  solicitante           :string(100)      not null
#  notifica_responsavel  :boolean          default(FALSE), not null
#  ano                   :integer          not null
#  sequencia             :integer          not null
#  user_id               :integer
#  bloqueado             :boolean          default(FALSE)
#

class Rac < ActiveRecord::Base

  belongs_to :auditoria
  belongs_to :funcionario
  belongs_to :tipo_rac
  belongs_to :area
  belongs_to :tipo_nc
  belongs_to :user
  has_many   :rac_historicos, dependent: :destroy
  has_many   :rac_nao_conformidades
  has_and_belongs_to_many :funcionarios, join_table: :racs_participantes, dependent: :destroy, class_name: 'Funcionario'
  accepts_nested_attributes_for :rac_nao_conformidades, allow_destroy: true, reject_if: :all_blank
  has_many :rac_followups


  def verifica_adiamento
    if self.nova_dt_prevista.present?
      dt_anterior = self.dt_prevista_correacao
      antigo_responsavel = self.responsavel
      self.dt_prevista_correacao = self.nova_dt_prevista
      self.responsavel = self.NovoResponsavel

      #salva o log de alteração
      hist                   = RacHistorico.new
      hist.rac               = self
      hist.DtAnterior        = dt_anterior
      hist.NovaDt            = self.nova_dt_prevista
      hist.justificativa     = self.justificativa
      hist.AntigoResponsavel = antigo_responsavel
      hist.NovoResponsavel   = self.responsavel
      hist.historico         = "Ação corretiva adiada."
      hist.user              = current_user
      hist.save!

      #limpa os campos
      self.nova_dt_prevista = nil
      self.NovoResponsavel  = nil
      self.justificativa    = nil
    end
  end

  def self.check_atraso

    limite_maximo = 90 #Quantidade de dias onde o status será alterado

    where("dt_real_encerramento is ? and bloqueado = ?", nil, false).each do |rac|

      qtd_dias   = self.dias_entre(Time.now.to_date, rac.dt_criacao.to_date)
      remetentes = Funcionario.ativos.where("grupo_id = ? or nome = ?", 1, rac.responsavel)

      if(qtd_dias <= limite_maximo)
        RacMailer.rac_notificacao_atraso(qtd_dias, remetentes).deliver_later if ((qtd_dias == 30) || (qtd_dias == 60) || (qtd_dias == 90))
      else
        #bloqueia a rac
        self.bloqueio rac.id

        # Dados que serao enviados para o email
        dados = Hash.new
        dados[:numero] = zero(rac.numero) + rac.ano
        dados[:atraso] = qtd_dias

        remetentes.each do |f|
          RacMailer.rac_bloqueio(f.email, dados).deliver_later
        end
      end
    end
  end

  private

    def self.dias_entre(d1, d2)
      (d1 - d2).to_i / (24 * 60 * 60)
    end

    def self.bloqueio(id)
      rac = find id
      rac.bloqueado = !rac.bloqueado if rac.bloqueado.present?
      rac.save
    end

end
