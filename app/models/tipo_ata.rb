# == Schema Information
#
# Table name: tipo_atas
#
#  id         :integer          not null, primary key
#  descricao  :string(80)       not null
#  created_at :datetime
#  updated_at :datetime
#

class TipoAta < ActiveRecord::Base
	has_many :reunioes
end
