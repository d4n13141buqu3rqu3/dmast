# == Schema Information
#
# Table name: helpers
#
#  id         :integer          not null, primary key
#  titulo     :string(40)
#  corpo      :text
#  versao     :string(5)
#  created_at :datetime
#  updated_at :datetime
#  user_id    :integer
#

class Helper < ActiveRecord::Base
  belongs_to :user
end
