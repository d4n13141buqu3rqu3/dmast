class AnexoDocsController < ApplicationController
  before_filter :require_user
  def new
  end

  def index
  end

  def create
  	@anexo = AnexoDoc.new(anexo_params)
    doc = Documento.find(@anexo.documento_id)
    DocumentoHistorico.create documento: doc, historico: "Arquivo anexado por #{current_user.name}"
    @anexo.documento_id = params[:doc_id]
    @anexo.user         = current_user
    @anexo.save
    redirect_to "/atualizar-documento-#{doc.id}"  , notice: 'Arquivo enviado com sucesso!'
  end

  private
    def anexo_params
    	params.require(:anexo_doc).permit(:anexo, :descricao, :documento_id, :doc_id)
    end
end
