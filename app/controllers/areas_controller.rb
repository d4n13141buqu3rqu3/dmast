class AreasController < ApplicationController
  before_action :set_area, only: [:show, :edit, :update, :destroy, :atualizar]
  before_filter :require_user
  def index
    @areas = Area.all.order(:descricao)
    @area = Area.new
  end

  def show
  end

  def create
    @area = Area.new(area_params)
    @area.save
    redirect_to areas_de_atuacao_path, notice: 'Cadastro realizado!'
  end

  def atualizar
  end

  def update
    @area.update(area_params)
    redirect_to areas_de_atuacao_path, notice: 'Registro atualizado!'
  end

  def destroy
    @area.destroy
    redirect_to  areas_de_atuacao_path, notice: 'Excluído!'
  end

  private
    def set_area
      @area = Area.find(params[:id])
    end

    def area_params
      params.require(:area).permit(:descricao, :email)
    end
end
