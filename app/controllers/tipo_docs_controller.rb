class TipoDocsController < ApplicationController
  before_action :set_tipo_doc, only: [:show, :edit, :update, :destroy, :atualizar]
  before_filter :require_user
  def index
    @tipos = TipoDoc.all.order(:descricao)
    @tipo = TipoDoc.new
  end

  def atualizar
  end


  def show
  end

  def new
    @tipo_doc = TipoDoc.new
  end

  def edit
  end

  def create
    @tipo_doc = TipoDoc.new(tipo_doc_params)
    if @tipo_doc.save
      redirect_to tipos_de_docs_path, notice: 'Salvo com sucesso!'
    else
      render :new
    end
  end

  def update
    if @tipo_doc.update(tipo_doc_params)
      redirect_to tipos_de_docs_path, notice: 'Atualizado com sucesso!'
    else
      render :edit
    end
  end

  def destroy
    @tipo_doc.destroy
    redirect_to tipos_de_docs_path, notice: 'Excluído com sucesso!'
  end

  private
    def set_tipo_doc
      @tipo_doc = TipoDoc.find(params[:id])
    end

    def tipo_doc_params
      params.require(:tipo_doc).permit(:descricao)
    end
end
