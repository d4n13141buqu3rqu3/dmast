class RqasController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_filter :require_user
  def index
    @rqas = Rqa.all
  end

  def incluir
    @areas = Area.all
  end

  def salvar
    Rqa.add(params)
    redirect_to "/rqa/", notice: 'Relato de quase acidente foi salvo e enviado ao SESMET.'
    #else
    #  redirect_to "/rqa/", notice: 'Não foi possível salvar o registro'
    #end
  end
end
