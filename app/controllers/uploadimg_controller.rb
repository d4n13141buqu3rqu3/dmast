class UploadimgController < ApplicationController
  respond_to :json
  before_filter :require_user
  def create
    geometry = Paperclip::Geometry.from_file params[:file]
    image    = Image.create params.permit(:file, :alt, :hint)

    render json: {
        image: {
          url:    image.file.url,
          height: geometry.height.to_i,
          width:  geometry.width.to_i
        }
    }, layout: false, content_type: "text/html"

  end



end