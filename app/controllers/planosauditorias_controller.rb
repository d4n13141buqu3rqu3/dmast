class PlanosauditoriasController < ApplicationController
  before_action :set_planoauditoria, only: [:show, :edit, :update, :destroy, :atualizar]
  layout false, only: :ajax_planos
  before_filter :require_user

  def ajax_planos
    @planos = Planoauditoria.includes(:auditorias).order(:nome)
  end

  def destinatarios
    @auditoria = Planoauditoria.find params[:id]
    @usuarios = @auditoria.usuarios
    @ids = []

    @usuarios.each do |u|
      @ids << u.id
    end

    @usuarios = Usuario.where.not(id: @ids)

    NotificacaoMailer.msg(nil).deliver
  end

  def notificar_destinatarios
    if params[:id_plano].present?
      plano = Planoauditoria.find params[:id_plano]
      plano.usuarios.each do |u|
        NotificacaoMailer.msg(u.email).deliver
      end
    end
  end


  def index
    @planosauditorias = Planoauditoria.all.order("created_at desc")
    @plano = Planoauditoria.new
    @funcionarios = Funcionario.ativos
  end

  def atualizar
    @funcionarios = Funcionario.ativos
  end

  def show
  end

  def new
    @planoauditoria = Planoauditoria.new
  end

  def edit
  end

  def create
    @planoauditoria = Planoauditoria.new(planoauditoria_params)
    if @planoauditoria.save
      redirect_to planos_de_auditorias_path, notice: 'Plano de auditoria salvo com sucesso'
    else
      render :new
    end
  end


  def update
    if @planoauditoria.update(planoauditoria_params)
      #Atualiza as auditorias filhas desse plano de auditoria
      @planoauditoria.auditorias.where(status: "Em andamento").update_all status: 'Finalizado' #unless @planoauditoria.encerrado != 1
      redirect_to planos_de_auditorias_path, notice: 'Plano de auditoria atualizado com sucesso.'
    else
      render :edit
    end
  end


  def destroy
    @planoauditoria.destroy
    redirect_to planos_de_auditorias_path, notice: 'Plano de auditoria deletado com sucesso''.'
  end

  private
    def set_planoauditoria
      @planoauditoria = Planoauditoria.find(params[:id])
    end

    def planoauditoria_params
      params.require(:planoauditoria).permit(:nome, :encerrado, :prazo, :ano, {funcionario_ids: []}, :data_inicio, :data_fim)
    end
end
