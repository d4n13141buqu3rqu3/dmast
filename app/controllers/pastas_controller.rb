class PastasController < ApplicationController
	before_filter :require_user

	def index
		@pastas = Pasta.order(:descricao).includes(:area)
		@pasta = Pasta.new
	end

	def create
		@pasta = Pasta.new(pasta_params)
    	@pasta.save
    	redirect_to :back, notice: 'Cadastro realizado!'
	end

	def atualizar
		@pasta = Pasta.find(params[:id])
	end

	def update
		@pasta = Pasta.find(params[:id])
		@pasta.update(pasta_params)
    	redirect_to pastas_path, notice: 'Registro atualizado!'
	end

	private
	def set_pasta
		@pasta = Pasta.find params[:id]
	end

	def pasta_params
		params.require(:pasta).permit(:descricao, :area_id)
	end
end
