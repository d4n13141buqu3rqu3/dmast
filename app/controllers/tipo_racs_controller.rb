class TipoRacsController < ApplicationController
  before_action :set_tipo_rac, only: [:show, :edit, :update, :destroy, :atualizar]
  before_filter :require_user
  def atualizar

  end

  def index
    @tipos = TipoRac.all.order :descricao
    @tipo = TipoRac.new
  end

  def show
  end

  def new
    @tipo_rac = TipoRac.new
  end

  def edit
  end

  def create
    @tipo_rac = TipoRac.new(tipo_rac_params)

    if @tipo_rac.save
      redirect_to tipos_de_racs_path, notice: 'Tipo de RAC salvo com sucesso!'
    else
      render :new
    end
  end

  def update
    if @tipo_rac.update(tipo_rac_params)
      redirect_to tipos_de_racs_path, notice: 'Tipo de RAC atualizado com sucesso!.'
    else
      render :edit
    end
  end

  def destroy
    @tipo_rac.destroy
    redirect_to tipos_de_racs_path, notice: 'Tipo de RAC excluído com sucesso!'
  end

  private
    def set_tipo_rac
      @tipo_rac = TipoRac.find(params[:id])
    end

    def tipo_rac_params
      params.require(:tipo_rac).permit(:descricao)
    end
end
