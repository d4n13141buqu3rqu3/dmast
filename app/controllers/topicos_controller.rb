class TopicosController < ApplicationController
  before_action :set_topico, only: [:show, :edit, :update, :destroy]
  before_filter :require_user
  def index
    @topicos = Topico.all
  end

  def show
  end

  def new
    @topico = Topico.new
  end

  def add
    @topico = Topico.new
    @reuniao = params[:reuniao_id]
  end

  def edit
  end

  def create
    @topico = Topico.new(topico_params)
    if @topico.save
      redirect_to @topico, notice: 'Topico was successfully created.'
    else
      render :new
    end
  end

  def update
    if @topico.update(topico_params)
      redirect_to @topico, notice: 'Topico was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @topico.destroy
    redirect_to topicos_url, notice: 'Topico was successfully destroyed.'
  end

  private
    def set_topico
      @topico = Topico.find(params[:id])
    end

    def topico_params
      params.require(:topico).permit(:descricao, :encerrado, :reuniao_id)
    end
end
