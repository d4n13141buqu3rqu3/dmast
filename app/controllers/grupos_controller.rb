class GruposController < ApplicationController

  before_filter :require_user
  def index
    @grupos = Grupo.all.order :descricao
  end
end
