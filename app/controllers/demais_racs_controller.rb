class DemaisRacsController < ApplicationController
  before_action :set_rac, only: [:update, :show, :atualizar, :print]
  skip_before_action :verify_authenticity_token
  before_filter :require_user
  def print

  end

  def destroy
    rac = DemaisRac.find params[:id]
    rac.destroy
    redirect_to :back, notice: "Excluido com sucesso"
  end

  def show
  end

  def atualizar
  end

  def index
    @racs = DemaisRac.includes(:area_solicitante, :area_responsavel)
  end

  def inserir
    @rac = DemaisRac.new
  end

  def create
    @rac = DemaisRacRepository.save(rac_params)
    redirect_to "/demais-racs", notice: 'Relatório registrado'
  end

  def update
    @rac.update(rac_params)

    if @rac.nova_dt_prevista.present?
      dt_anterior = @rac.dt_acao_corretiva
      antigo_responsavel = @rac.responsavel_acao_corretiva
      @rac.dt_acao_corretiva = @rac.nova_dt_prevista
      @rac.responsavel_acao_corretiva = @rac.NovoResponsavel

      #salva o log de alteração
      hist                   = DemaisRacHistorico.new
      hist.demais_rac        = @rac
      hist.DtAnterior        = dt_anterior
      hist.NovaDt            = @rac.nova_dt_prevista
      hist.justificativa     = @rac.just_nova_dt
      hist.AntigoResponsavel = antigo_responsavel
      hist.NovoResponsavel   = @rac.NovoResponsavel
      hist.historico         = "Ação corretiva adiada."
      hist.user              = current_user
      hist.save!

      #limpa os campos
      @rac.nova_dt_prevista = nil
      @rac.NovoResponsavel = nil
      @rac.just_nova_dt = nil
      @rac.save
    end

    if @rac.dt_real_encerramento.present?
      @rac.status = "Finalizado"
      @rac.save
    end

    redirect_to :back, notice: "Relatório atualizado"
  end

  private
    def rac_params
      params.require(:demais_rac).permit(:status, :status_correcao, :tipo_reclamacao_id, :area_solicitante_id, :area_responsavel_id, :user_id, :causa_raiz, :desc_acao_corretiva, :dt_acao_corretiva, :nova_dt_prevista, :just_nova_dt, :responsavel_acao_corretiva, :anal_critica, :dt_real_encerramento, :tipo_nc_id, :desc_nc, :solicitante, :dt_criacao, :NovoResponsavel)
    end

    def set_rac
      @rac = DemaisRac.find(params[:id])
    end


end
