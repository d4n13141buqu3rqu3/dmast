class TipoAtasController < ApplicationController
  before_action :set_tipo_atum, only: [:show, :edit, :update, :destroy, :atualizar]
  before_filter :require_user
  def index
    @tipos = TipoAta.all.order(:descricao)
    @tipo = TipoAta.new
  end

  def show
  end

  def new
    @tipo = TipoAta.new
  end

  def atualizar

  end

  def edit
  end

  def create
    @tipo = TipoAta.new(tipo_atum_params)
    if @tipo_atum.save
      redirect_to tipos_de_atas_path, notice: 'Cadastro realizado'
    else
      render :new
    end
  end

  def update
    if @tipo.update(tipo_atum_params)
      redirect_to tipos_de_atas_path, notice: 'Registro atualizado'
    else
      render :edit
    end
  end

  def destroy
    @tipo.destroy
    redirect_to tipos_de_atas_path, notice: 'Registro excluído'
  end

  private
    def set_tipo_atum
      @tipo = TipoAta.find(params[:id])
    end

    def tipo_atum_params
      params.require(:tipo_ata).permit(:descricao)
    end
end
