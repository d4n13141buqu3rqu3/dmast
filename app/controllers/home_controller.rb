class HomeController < ApplicationController
  before_filter :require_user
  def index
    @auditorias = Auditoria.pendentes_principais
  end

end
