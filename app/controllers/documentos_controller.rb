class DocumentosController < ApplicationController
  before_action :set_documento, only: [:edit, :update, :destroy, :visualizar]
  before_action :set_text_area, only: [:edit, :new, :criar_versao]
  before_filter :require_user
  skip_before_filter :verify_authenticity_token

  #Reenvia o workflow para consensadores ou homolgadores
  def reenviar_workflow
    if request.get?
      @documento = Documento.find(params[:id])
      if @documento.status == 'Enviado para consenso'
        @grp = GrpConsensador.find @documento.grp_consenso_id
        @texto = 'Consensadores'
      else
        @grp = GrpHomologador.find @documento.grp_consenso_id
        @texto = 'Homologadores'
      end  
    else
      params[:url] = @URL
      DocumentoRepository.reenviar_workflow(params)
      redirect_to :back, notice: "E-Mail reencaminhado."
    end
  end

  #Documentos excluídos
  def lixeira
    @documentos = Documento.lixo
  end

  #Envia documento para lixeira
  def enviar_lixeira
    DocumentoRepository.excluir(params)
    redirect_to :back, notice: "Documento enviado para a lixeira!"
  end

  #Restaura documento da lixeira
  def restaurar_lixeira
    DocumentoRepository.restaurar(params)
    redirect_to :back, notice: "Documento restaurado com sucesso!"
  end

  #Download de anexo
  def download_anexo
    path = File.join(Rails.root, "public")
    anexo = AnexoDoc.find params[:id]
    send_file(File.join path, anexo.anexo_url)
  end

  def index
    if params[:filtro].present?
      filtro
      @op = params[:filtro]
      return
    end
    redirect_to root_path
  end

  def criar_versao
    @old = Documento.find(params[:id])
    @grp_consensadores = GrpConsensador.all
    @grp_homologadores = GrpHomologador.all
    
    @documento = Documento.new do |doc|
      doc.versao_anterior = @old.versao
      doc.titulo = @old.titulo
      doc.sigla = @old.sigla
      doc.tipoDoc_id = @old.tipoDoc_id
      doc.link_versao_anterior = "#{@URL}/visualizar-documento-#{@old.id}"
      doc.dias_consenso = @old.dias_consenso
      doc.dias_homologar = @old.dias_homologar
      doc.texto_doc = @old.texto_doc
      doc.status = "Em elaboraçao"
      doc.doc_pai_id = @old.id
      doc.areas = @old.areas
    end
    
  end

  def add_doc_complementar
    if request.post?
      DocumentoComplementar.create doc_filho_id: params[:doc_filho_id], doc_pai_id: params[:doc_pai_id]
      documento = Documento.find params[:doc_pai_id]
      DocumentoHistorico.create documento: documento, historico: "Documento complementar #{documento.titulo} adicionado"
      return redirect_to "/atualizar-documento-#{params[:doc_pai_id]}", notice: "Operação realizada com sucesso."
    end
    @doc_id = params[:id]
  end

  def ver_oficial
    @documento = Documento.find params[:id]
    @complementares = DocumentoComplementar.where doc_pai_id: @documento.id
    @doc_titulo = []
    @complementares.each do |d|
      @doc_titulo << Documento.find(d.doc_filho_id).sigla
    end
    @consensadores = Consensamento.where documento_id: @documento.id
    @homologadores = Homologacao.where documento_id: @documento.id
    @localizacoes = DocumentoLocalizacao.where documento_id: @documento.id
  end

  def ver
    @documento = Documento.find params[:id]
    @complementares = DocumentoComplementar.where doc_pai_id: @documento.id
    @doc_titulo = []
    @complementares.each do |d|
      @doc_titulo << Documento.find(d.doc_filho_id).sigla
    end
    @consensadores = Consensamento.where documento_id: @documento.id
    @homologadores = Homologacao.where documento_id: @documento.id
    @localizacoes = DocumentoLocalizacao.where documento_id: @documento.id
  end

  def visualizar_via_email
    @documento = Documento.find params[:id]
    @consenso = Consensamento.where("documento_id = ? AND email = ? AND status = ?", @documento.id, current_user.email, "Resposta pendente")
    @consenso = @consenso.first
  end

  def anexar
    @doc_id = params[:doc_id]
    @anexo = AnexoDoc.new
  end

  def recebe_resposta_homologacao
    params[:user] = current_user
    params[:url] = @URL
    resp = DocumentoRepository.homologacao(params)
    return redirect_to root_path, notice: "Resposta enviada" if resp.homologado == false
    return redirect_to "/visualizar-documentos-oficial", notice: "Resposta enviada"
  end

  def recebe_resposta_consenso
    params[:url] = @URL
    resp = DocumentoRepository.consenso(params)
    #Caso não tenha sido consensado
    if resp.consensado == false
      redirect_to "/visualizar-documentos-consenso", notice: "Resposta enviada"
    else
      redirect_to "/visualizar-documentos-homologacao", notice: "Resposta enviada"
    end
  end

  def homologar
    @documento = Documento.find params[:id]
    @homologacao = Homologacao.where("documento_id = ? and email = ? and status = ?", params[:id], current_user.email, "Resposta pendente").first
  end

  def consensar
    @id            = params[:id]
    doc            = Documento.find @id
    if doc.present?
      grp            = GrpConsensador.find doc.grp_consenso_id
      @consensadores = grp.funcionarios
    else
      redirect_to :back, notice: "Ocorreu algum erro interno ao encontrar o documento, contate a T.I"
    end
  end

  def enviar_consenso
    params[:url] = @URL
    doc = DocumentoRepository.enviar_para_consenso(params)
    redirect_to "/atualizar-documento-#{doc.id}", notice: 'Worklow de consenso iniciado, aguarde a resposta dos consensadores'
  end

  def new
    @documento         = Documento.new
    @grp_consensadores = GrpConsensador.all
    @grp_homologadores = GrpHomologador.all
  end

  def edit
    @historico = DocumentoHistorico.where(documento_id: params[:id])
    @complementares = DocumentoComplementar.where("doc_pai_id = ?", params[:id])
    @grp_consensadores = GrpConsensador.all
    @grp_homologadores = GrpHomologador.all
  end

  def create
    @documento = DocumentoRepository.salva(documento_params)
    redirect_to "/atualizar-documento-#{@documento.id}", notice: 'Documento salvo.'
  end

  def update
    if params[:op] == 'consensar'
      @documento.dt_envio_consenso = Time.now
    end
    @documento.update(documento_params)
    redirect_to "/atualizar-documento-#{@documento.id}", notice: 'O documento foi atualizado.'
  end


  def destroy
    @documento.destroy
    redirect_to :back, notice: 'Documento excluído do banco de dados'
  end

  def visualizar
  end

  private

    def filtro
      case params[:filtro]
        when "elaboracao"
          @filtro = 'Elaboração'
          @documentos = Documento.documentos_ativos "Em elaboração"
        when "consenso"
          @filtro      = 'Consenso'
          @documentos  = Documento.documentos_ativos "Enviado para consenso"
        when "homologacao"
          @filtro      = "Homologação"
          @documentos  = Documento.documentos_ativos "Enviado para homologação"
        when "oficial"
          @filtro     = 'Documentos oficiais'
          @visualizar = true
          @documentos = Documento.documentos_ativos
        else
          redirect_to root_path, notice: "Boa tentativa..."
      end
    end

    def set_text_area
      @text_area_doc = true
    end

    def set_documento
      @documento = Documento.find(params[:id])
    end

    def documento_params
      params.require(:documento).permit(
        :nome, :elaborador, :user_id, :titulo, :tipoDoc_id, :status,
        :texto_doc, :dias_consenso, :dias_homologar,
        :dt_envio_consenso, :dt_envio_homologacao, :sigla, :nome_original,
        :versao, :versao_anterior, :id_doc_anterior, :link_versao_anterior,
        {area_ids: []}, :grp_consensador_id, :grp_homologador_id, :grp_consenso_id, :doc_pai_id, :link_versao_anterior, :versao_anterior
      )
    end
end
