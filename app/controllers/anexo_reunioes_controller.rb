class AnexoReunioesController < ApplicationController
  before_filter :require_user
  def new
  end

  def create
    @anexo = AnexoReuniao.new(anexo_params)
    @anexo.reuniao_id = params[:reuniao_id]
    @anexo.user = current_user
    @anexo.save
    redirect_to "/reunioes/#{params[:reuniao_id]}"  , notice: 'Arquivo enviado com sucesso!'
  end

  private
    def anexo_params
      params.require(:anexo_reuniao).permit(:anexo, :descricao)
    end
end
