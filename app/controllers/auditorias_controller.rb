class AuditoriasController < ApplicationController
  before_action :set_auditoria, only: [:destroy, :update, :show]
  skip_before_action :verify_authenticity_token
  before_filter :require_user

  def index
    @planoauditoria = Planoauditoria.find(params[:id])
    @auditoria = Auditoria.new
    @areas = Area.all.order(:descricao)
  end

  def followup
    @auditoria = Auditoria.find params[:id]
  end

  def disparar_followup
    auditoria = Auditoria.find(params[:id])

    destinatarios = []
    destinatarios << auditoria.auditor.email

    auditoria.participantes.each do |p|
      destinatarios << p.email
    end

    dados = {
      numero:        auditoria.numero,
      area:          auditoria.area.descricao,
      auditor:       auditoria.auditor.nome,
      status:        auditoria.status,
      duracao:       auditoria.duracao_prevista,
      data_prevista: auditoria.data_prevista,
      hora_prevista: auditoria.hora_prevista
    }

    logger.info destinatarios
    logger.info dados

    Thread.new {
      destinatarios.each do |d|
        logger.info d
        WorkflowMailer.enviar_followup(d, dados).deliver
      end
    }

    redirect_to root_path, notice: "Follow enviado..."
  end

  def salva_atividade
    AuditoriaAtividade.create auditoria_id: params[:auditoria_id], atividade_id:  params[:descricao]
    redirect_to :back, notice: "Atividade auditoria incluída com sucesso !"
  end

  def add_atividade
    @auditoria = Auditoria.find params[:id]
  end

  def del_atividade
    a = AuditoriaAtividade.find(params[:id])
    a.destroy
    redirect_to :back
  end

  def lista
    @planoauditoria = Planoauditoria.find(params[:id])
  end

  def imprimir
    @auditoria = Auditoria.find params[:id]
  end

  def atualizar
    @auditoria = Auditoria.find params[:id]
    @areas = Area.all.order(:descricao)
  end

  def visualizar

  end

  def update
    if @auditoria.update(auditoria_params)
      redirect_to "/auditorias,plano,#{@auditoria.planoauditoria.id}", notice: 'Auditoria atualizada com sucesso.'
    else
      render :edit
    end
  end

  def create
    @auditoria = Auditoria.new(auditoria_params)
    @auditoria.planoauditoria_id = params[:planoauditoria_id]
    @auditoria.criado_por = current_user
    @plano_id = params[:planoauditoria_id]
    @auditoria.save
    redirect_to "/auditorias,plano,#{@auditoria.planoauditoria.id}", notice: 'Auditoria salva com sucesso!'
  end

  def show
  end

  def destroy
    @auditoria = Auditoria.find params[:id]
    @auditoria.destroy
    @auditoria.save
    redirect_to '/auditorias,plano,#{params[:planoauditoria_id]}', notice: 'Auditoria salva com sucesso!'
  end

  private

    def auditoria_params
      params.require(:auditoria).permit(
          :numero, :auditor_lider_id, :area_id,
          :data_prevista, :status, :planoauditoria_id,
          :ano, :hora_prevista, :duracao_prevista,
          :legislacao, {participante_ids: []}, {atividade_ids: []}, :auditoria_id, :auditoria
      )
    end

    def set_auditoria
      @auditoria = Auditoria.find(params[:id])
    end

end
