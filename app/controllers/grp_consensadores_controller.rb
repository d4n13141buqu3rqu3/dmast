class GrpConsensadoresController < ApplicationController
  before_action :set_grupo, only: [:update]
  before_filter :require_user
  def index
    @grupos       = GrpConsensador.includes(:funcionarios )
    @grupo        = GrpConsensador.new
    @funcionarios = Funcionario.ativos_com_email
  end

  def create
    grp = GrpConsensador.new(grp_params)
    grp.save!
    redirect_to :back, notice: "Registro salvo com sucesso"
  end

  def edit
    @funcionarios = Funcionario.ativos_com_email
    @grupo = GrpConsensador.find params[:id]
  end

  def update
    @grupo.update(grp_params)
    redirect_to "/grupo-consensadores/", notice: "Atualizado com sucesso"
  end

  def destroy
    @grupo = GrpConsensador.find params[:id]
    @grupo.destroy
    redirect_to :back, notice: "Grupo excluído com sucesso"
  end

  private
    def grp_params
      params.require(:grp_consensador).permit(:descricao, {funcionario_ids: []})
    end

    def set_grupo
      @grupo = GrpConsensador.find params[:grp_consensador][:id]
    end


end
