class RapsController < ApplicationController
  before_action :set_rap, only: [:show, :edit, :update, :destroy, :atualizar, :alterar_data_acao, :print]
  skip_before_action :verify_authenticity_token
  before_filter :require_user
  def followup_atualiza
    @rap = RapFollowup.find(params[:id])
  end

  def followup_atualiza_salva

  end

  def followup_remove
    RapFollowup.find(params[:id]).destroy
    redirect_to :back
  end

  def followup_lista
    @rap = Rap.find params[:id]
    redirect_to :back unless @rap.present?
  end

  def followup_salvar
    RapFollowup.create do |f|
      f.user_id = params[:user_id],
          f.rap_id = params[:id],
          f.dt_followup = params[:dt_followup],
          f.hora = params[:hora],
          f.finalizado = params[:finalizado],
          f.inf_final = params[:inf_final],
          f.inf_inicial = params[:inf_inicial],
          f.responsavel = params[:responsavel]
    end
    redirect_to :back, notice: "Salvo com sucesso"
  end
  def print

  end

  def index
    @raps = Rap.all
  end

  def atualizar
  end

  def alterar_data_acao

  end

  def show

  end

  def lista
    @raps       = Rap.all
  end


  def new
    @rap = Rap.new
  end


  def edit
  end


  def create
    @rap = RapRepository.save(rap_params)
    redirect_to "/raps/", notice: "Relatório salvo"
  end

  def update
    if @rap.update(rap_params)
      redirect_to :back, notice: 'RAP atualizada'
    else
      render :edit
    end
  end

  def destroy
    @rap.destroy
    redirect_to "/raps", notice: 'RAP foi excluída'
  end

  private
    def set_rap
      @rap = Rap.find(params[:id])
    end

    def rap_params
      params.require(:rap).permit(:user_id, :status, :tipo_reclamacao_id, :area_responsavel_id, :area_solicitante_id, :solicitante, :tipo_nc_id, :desc_nc, :status_acao, :desc_acao, :consequencia, :dt_prevista, :nova_dt, :justificativa, :analise_critica, :dt_encerramento, :usuario_id, :inf, {usuario_ids: []}, :dt_criacao)
    end
end
