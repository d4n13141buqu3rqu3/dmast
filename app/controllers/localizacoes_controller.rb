class LocalizacoesController < ApplicationController
	before_filter :require_user

	def index
		@localizacoes = Localizacao.all
		@localizacao = Localizacao.new
	end

	def create
		@localizacao = Localizacao.new(localizacao_params)
		@localizacao.save
		redirect_to :back, notice: "Cadastro realizado"
	end

	def atualizar
		@localizacao = Localizacao.find(params[:id])
	end

	def update
		@localizacao = Localizacao.find(params[:id])
		@localizacao.update(localizacao_params)
    	redirect_to localizacoes_path, notice: 'Registro atualizado!'
	end

	private
	def localizacao_params
		params.require(:localizacao).permit(:descricao, :numero_id)
	end
end
