class TipoNcsController < ApplicationController
  before_action :set_tipo_nc, only: [:show, :edit, :update, :destroy, :atualizar]
  before_filter :require_user
  def index
    @tipos= TipoNc.paginate(:page => params[:page], :per_page => 10).order(:descricao)
    @tipo = TipoNc.new
  end

  def atualizar

  end

  def show
  end

  def new
    @tipo_nc = TipoNc.new
  end

  def edit
  end

  def create
    @tipo_nc = TipoNc.new(tipo_nc_params)

    if @tipo_nc.save
      redirect_to tipos_de_ncs_path, notice: 'Tipo de não conformidade salva com sucesso!'
    else
      render :new
    end
  end

  def update
    if @tipo_nc.update(tipo_nc_params)
      redirect_to tipos_de_ncs_path, notice: 'Tipo de não conformidade atualizada com sucesso!'
    else
      render :edit
    end
  end

  def destroy
    @tipo_nc.destroy
    redirect_to tipos_de_ncs_path, notice: 'Tipo de não conformidade excluída com sucesso!'
  end

  private
    def set_tipo_nc
      @tipo_nc = TipoNc.find(params[:id])
    end

    def tipo_nc_params
      params.require(:tipo_nc).permit(:sigla, :descricao)
    end
end
