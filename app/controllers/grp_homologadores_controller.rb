class GrpHomologadoresController < ApplicationController
  before_filter :set_grupo, only: [:update]
  before_filter :require_user
  def index
    @grupos       = GrpHomologador.includes(:funcionarios)
    @grupo        = GrpHomologador.new
    @funcionarios = Funcionario.ativos_com_email
  end

  def create
    grp = GrpHomologador.new(grp_params)
    grp.save!
    redirect_to :back, notice: "Registro salvo com sucesso"
  end

  def destroy
    @grupo = GrpHomologador.find params[:id]
    @grupo.destroy
    redirect_to :back, notice: "Grupo excluído com sucesso"
  end

  def edit
    @funcionarios = Funcionario.ativos_com_email
    @grupo = GrpHomologador.find params[:id]
  end

  def update
    @grupo.update(grp_params)
    redirect_to "/grupo-homologadores/", notice: "Atualizado com sucesso"
  end
  private
    def grp_params
      params.require(:grp_homologador).permit(:descricao, {funcionario_ids: []}, :id)
    end

    def set_grupo
      @grupo = GrpHomologador.find params[:grp_homologador][:id]
    end
end
