class ReunioesController < ApplicationController
  before_action :set_params, only: [:atualizar, :destroy, :update, :show, :anexar]
  before_filter :require_user
  def index
    @reunioes = Reuniao.all.order('created_at desc')
    @reuniao = Reuniao.new
  end

  def anexar
    @reuniao_id = params[:id]
    @anexo_reuniao = AnexoReuniao.new
  end

  def excluir_topico
    topico = Topico.find params[:id]
    topico.destroy!
    redirect_to :back
  end

  def marcar_finalizada
    topico = Topico.find params[:id]
    topico.encerrado = true
    topico.save!
    redirect_to :back
  end

  def salva_topico
    novo_topico = Topico.new
    novo_topico.descricao = params[:descricao]
    novo_topico.reuniao_id = params[:reuniao_id]
    novo_topico.encerrado = false
    novo_topico.save!
    redirect_to "/reunioes/#{params[:reuniao_id]}", notice: 'Tópico incluído com sucesso'
  end

  def atualizar
    @tipo = TipoAta.all.order :descricao
    @areas = Area.all.order :descricao
  end

  def incluir
    @tipo = TipoAta.all.order :descricao
    @areas = Area.all.order :descricao
    @tipo = TipoAta.all.order :descricao
    @reuniao = Reuniao.new
  end

  def destroy
    @reuniao.destroy
    redirect_to reunioes_path, notice: 'Ata de reunião excluída com sucesso.'
  end

  def download_anexo
    path = File.join(Rails.root, "public")
    anexo = AnexoReuniao.find params[:id]
    anexo.downloads += 1
    anexo.save!
    send_file(File.join path, anexo.anexo_url)
  end

  def show
    @tipo   = TipoAta.all.order :descricao
    @areas  = Area.all.order :descricao
    @topico =  Topico.new
  end

  def encerrar_reuniao
    @reuniao = Reuniao.find params[:id]
    @reuniao.status = "Encerrada"
    @reuniao.save!
    redirect_to @reuniao
  end

  def create
    @reuniao = Reuniao.new(reuniao_params)

    if @reuniao.save
      redirect_to @reuniao, notice: 'Ata de reunião salva!'
    else
      render :new
    end
  end

  def update
    if @reuniao.update(reuniao_params)
      redirect_to @reuniao, notice: 'Ata de reunião atualizada com sucesso.'
    else
      render :edit
    end
  end

  private
    def reuniao_params
      params.require(:reuniao).permit(:assunto, :area_id, :data_reuniao, :TipoAta_id, :status, :descricao, {funcionario_ids: []}, :local_reuniao)
    end

    def set_params
      @reuniao = Reuniao.find params[:id]
    end

end
