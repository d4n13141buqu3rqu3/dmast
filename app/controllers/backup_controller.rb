class BackupController < ApplicationController
  before_filter :require_user
  
  def do
    system("rake db:seed:dump")
    path = File.join(Rails.root, "db")
    send_file(File.join path, "seeds.rb")
  end


end
