class HelpersController < ApplicationController
  before_action :set_helper, only: [:show, :edit, :update, :destroy]
  before_filter :require_user
  layout false, only: :ajax_ajuda

  def ajax_ajuda
    @helps = Helper.all
  end

  def index
    @helpers = Helper.all
    @helper = Helper.new
  end

  def show
  end

  def new
    @helper = Helper.new
  end

  def edit
  end

  def create
    @helper = Helper.new(helper_params)
    if @helper.save
      redirect_to helpers_url, notice: 'Help salvo!'
    else
      render :new
    end
  end

  def update
    if @helper.update(helper_params)
      redirect_to helpers_url, notice: 'Helpe atualizado!'
    else
      render :edit
    end
  end

  def destroy
    @helper.destroy
    redirect_to helpers_url, notice: 'Help excluído'
  end

  private
    def set_helper
      @helper = Helper.find(params[:id])
    end

    def helper_params
      params.require(:helper).permit(:titulo, :corpo, :versao, :user_id, :User)
    end
end
