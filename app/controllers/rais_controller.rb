class RaisController < ApplicationController
  before_action :set_rai, only: [:show, :edit, :update, :destroy, :atualizar, :print]
  skip_before_action :verify_authenticity_token
  before_filter :require_user
  def followup_atualiza
    @rai = RaiFollowup.find(params[:id])
  end

  def followup_atualiza_salva

  end

  def followup_remove
    RaiFollowup.find(params[:id]).destroy
    redirect_to :back
  end

  def followup_lista
    @rai = Rai.find params[:id]
    redirect_to :back unless @rai.present?
  end

  def followup_salvar
    RaiFollowup.create do |f|
      f.user_id = params[:user_id],
      f.rai_id = params[:id],
      f.dt_followup = params[:dt_followup],
      f.hora = params[:hora],
      f.finalizado = params[:finalizado],
      f.inf_final = params[:inf_final],
      f.inf_inicial = params[:inf_inicial],
      f.responsavel = params[:responsavel]
    end
    redirect_to :back, notice: "Salvo com sucesso"
  end



  def print
  end

  def index
    @planos = Planoauditoria.all.order :nome
    @rais   = Rai.all.order('created_at desc')
  end

  def atualizar
  end

  def defini_auditoria
    set_auditoria if params[:op].present? && params[:op] == '1'
  end

  def show

  end


  def new
    @rai = Rai.new
    set_auditoria
  end


  def edit
  end


  def create
    @rai = Rai.new(rai_params)
    if @rai.save
      redirect_to relatorios_de_auditorias_internas_path, notice: 'RAI criada com sucesso.'
    else
      render :new
    end
  end

  def update
    if @rai.update(rai_params)
      redirect_to rais_path, notice: 'RAI atualizada com sucesso.'
    else
      render :edit
    end
  end


  def destroy
    @rai.destroy
    redirect_to rais_url, notice: 'RAI excluída com sucesso.'
  end

  private

    def set_rai
      @rai = Rai.find(params[:id])
    end

    def set_auditoria
      @auditoria = Auditoria.find params[:auditoria_id]
    end

    def rai_params
      params.require(:rai).permit(:docs_ref, :auditoria_id, :objetivos, :resultado, :obs, :g_nconform, {funcionario_ids: []}, :user_id, :lider_id)
    end
end
