class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :inf_rodape, :defini_url_app, :notificacoes, :set_current_user

  helper_method :current_user, :require_user

  URL_DEV   = "http://localhost:3000"
  URL_PROD  = "http://dmast.amcel.com.br"
  EMAIL_ALL = "daniel.albuquerque@amcel.com.br"

  private
    def set_current_user
      User.current = current_user
    end

    def notificacoes
      if current_user.present?
        @qtd_notificacoes_pendentes = Notificacao.where(visto: false, notiemail: current_user.email).count
        @ultimas_tres_notificacoes  = Notificacao.where(visto: false, notiemail: current_user.email).take(5)
      end
    end

    def current_user
      @current_user ||= User.find(session[:user_id]) if session[:user_id]
    end

    def defini_url_app
      @URL = Rails.env.production? ? URL_PROD : URL_DEV
    end

    def inf_rodape
      @db_name   ||= Rails.configuration.database_configuration[Rails.env]["database"]
      @db_driver ||= ActiveRecord::Base.connection.instance_values["config"][:adapter].capitalize
    end

    def require_user
      auth_url = '/auth/google_oauth2'
      redirect_to auth_url unless session[:user_id] || @current_user
    end
end
