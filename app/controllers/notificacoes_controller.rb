class NotificacoesController < ApplicationController
  before_filter :require_user
  def visualizar
    Notificacao.find(params[:id]).update(visto: true)
    redirect_to n.url
  end

  def listar
  	@notificacoes = Notificacao.where("notiemail = ?", current_user.email).includes(:user).paginate(:page => params[:page], :per_page => 10).order(:created_at)
    @notificacoes.update_all(visto: true)
  end

  def pesquisar
    @notificacoes ||= Notificacao.where("descricao LIKE '%#{params[:p]}%'").paginate(:page => params[:page], :per_page => 10).order(:created_at)
    @busca        ||= params[:p]
  end

end
