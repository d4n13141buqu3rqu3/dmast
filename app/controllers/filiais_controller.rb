class FiliaisController < ApplicationController
  before_action :set_filial, only: [:show, :edit, :update, :destroy]
  before_filter :require_user
  def index
    @filiais = Filial.all
  end

  def show
  end

  def new
    @filial = Filial.new
  end

  def edit
  end

  def create
    @filial = Filial.new(filial_params)
    if @filial.save
      redirect_to @filial, notice: 'Filial criada com sucesso'
    else
      render :new
    end
  end

  def update
    if @filial.update(filial_params)
      redirect_to @filial, notice: 'Filial atualizada com sucesso'
    else
      render :edit
    end
  end

  def destroy
    @filial.destroy
    redirect_to filiais_url, notice: 'Filial excluída com sucesso'
  end

  private
    def set_filial
      @filial = Filial.find(params[:id])
    end
    
    def filial_params
      params.require(:filial).permit(:codigo, :nome)
    end
end
