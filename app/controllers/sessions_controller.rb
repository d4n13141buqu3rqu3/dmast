class SessionsController < ApplicationController

  layout 'sessao'
  DOMAIN = "amcel.com.br"
  MAIL   = "http://webmail.amcel.com.br/"

  def create
    user = User.from_omniauth(env["omniauth.auth"])
    dominio = user.email.split "@"
    if dominio[1] == DOMAIN
      funcionario = Funcionario.where email: user.email, ativo: true
      if funcionario.present?
        grupo = funcionario.find_by_email user.email
        session[:grupo]   = grupo.grupo.descricao
        session[:user_id] = user.id
        session[:email]   = user.email
        session[:nome]    = user.name
        current_user
      else
        return redirect_to sessions_nao_autorizado_path
      end
      return redirect_to root_path
    else
      return redirect_to sessions_nao_autorizado_path
    end
  end

  def destroy
    session[:user_id] = nil
    session[:grupo]   = nil
    session[:email]   = nil
    session[:nome]    = nil
    @current_user     = nil
    redirect_to MAIL
  end



  def nao_autorizado
  end
end
