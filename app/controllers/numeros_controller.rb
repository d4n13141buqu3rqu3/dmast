class NumerosController < ApplicationController
	before_filter :require_user

	def index
		@numeros = Numero.all
		@numero = Numero.new
	end

	def create
		@numero = Numero.new(numero_params)
		@numero.save
		redirect_to :back, notice: 'Cadastro realizado!'
	end

	def atualizar
		@numero = Numero.find(params[:id])
	end

	def update
		@numero = Numero.find(params[:id])
		@numero.update(numero_params)
    	redirect_to numeros_path, notice: 'Registro atualizado!'
	end

	def delete
	end

	private
	def set_numero
	end

	def numero_params
		params.require(:numero).permit(:descricao, :pasta_id)
	end
	
end
