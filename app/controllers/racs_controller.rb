class RacsController < ApplicationController
  before_action :set_rac, only: [:show, :edit, :update, :destroy, :atualizar, :alterar_data_acao, :print]
  skip_before_action :verify_authenticity_token
  before_filter :require_user
  
  def followup_atualiza
    @rac = RacFollowup.find(params[:id])
  end

  def followup_atualiza_salva

  end

  def followup_remove
    RacFollowup.find(params[:id]).destroy
    redirect_to :back
  end

  def followup_lista
    @rac = Rac.find params[:id]
    redirect_to :back unless @rac.present?
  end

  def followup_salvar
    RacFollowup.create do |f|
      f.user_id = params[:user_id],
          f.rac_id = params[:id],
          f.dt_followup = params[:dt_followup],
          f.hora = params[:hora],
          f.finalizado = params[:finalizado],
          f.inf_final = params[:inf_final],
          f.inf_inicial = params[:inf_inicial],
          f.responsavel = params[:responsavel]
    end
    redirect_to :back, notice: "Salvo com sucesso"
  end

  def index
    @racs       = Rac.all
    @auditorias = Auditoria.all.order 'created_at desc'
    @planos     = Planoauditoria.all
    @areas      = Area.all.order 'descricao desc'
    @tipo_rac   = TipoRac.all.order 'descricao desc'
  end

  def print
  end

  def show
  end


  def ver_adiamentos
    @adiamentos = RacHistorico.where rac_id: params[:id]
  end

  def adiar_correcao
  end

  def new
    @rac = Rac.new
    @auditoria = Auditoria.find params[:auditoria_id]
  end

  def edit
    @tipo_rac = TipoRac.all.order 'descricao desc'
  end

  def atualizar
  end

  def alterar_data_acao
  end

  def create
    @rac = RacRepository.save(rac_params)
    redirect_to "/racs", notice: 'RAC registrado com sucesso.'
  end


  def update
    @rac.update(rac_params)

    if @rac.nova_dt_prevista.present?
      dt_anterior = @rac.dt_prevista_correacao
      antigo_responsavel = @rac.responsavel
      @rac.dt_prevista_correacao = @rac.nova_dt_prevista
      @rac.responsavel = @rac.NovoResponsavel

      #salva o log de alteração
      hist = RacHistorico.new
      hist.rac = @rac
      hist.DtAnterior = dt_anterior
      hist.NovaDt = @rac.nova_dt_prevista
      hist.justificativa = @rac.justificativa
      hist.AntigoResponsavel = antigo_responsavel
      hist.NovoResponsavel = @rac.responsavel
      hist.historico = "Ação corretiva adiada."
      hist.user = current_user
      hist.save!

      #limpa os campos
      @rac.nova_dt_prevista = nil
      @rac.NovoResponsavel  = nil
      @rac.justificativa    = nil
      @rac.save
    end

    if @rac.dt_real_encerramento.present?
      @rac.status = "Finalizado"
      @rac.dt_real_execucao = @rac.dt_real_encerramento
      @rac.save
    end

    redirect_to :back, notice: 'RAC atualizado'
  end

  def destroy
    @rac.destroy
    redirect_to :back, notice: 'Rac deletada.'
  end

  private
    def set_rac
      @rac = Rac.find(params[:id])
    end

    def rac_params
      params.require(:rac).permit(
          :auditoria_id, :status,
          :responsavel,
          :analise_causa, :status_correcao, :desc_correacao,
          :dt_prevista_correacao, :nova_dt_prevista, :justificativa,
          :responsavel_correcao, :analise_eficacia, :dt_real_encerramento,
          :dt_real_execucao,{usuario_ids: []}, :responsavel, :desc_nc, :tipo_nc_id,
          :NovoResponsavel, :notifica_responsavel, :solicitante, :dt_criacao, :user_id
      )
    end
end
