module API
  module V1
    class Funcionarios < Grape::API
      version 'v1'
      format :json

      resources :funcionarios do
        desc "Retorna todos os funcionários"
        get do
          Funcionario.all
        end
      end

    end
  end
end
