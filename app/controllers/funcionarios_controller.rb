class FuncionariosController < ApplicationController
  before_action :set_usuario, only: [:destroy, :atualizar, :update]
  before_filter :require_user
  respond_to :json, :html

  def index
    if params[:search] 
      @funcionarios = Funcionario.where("nome like '%#{params[:search]}%' or email like '%#{params[:search]}%'").includes(:grupo).paginate(:page => params[:page], :per_page => 10).order(:nome)
    else
      @funcionarios = Funcionario.includes(:grupo).paginate(:page => params[:page], :per_page => 10).order(:nome)
    end

    if params[:filter]
      if params[:filter] == 'ativos'
        @funcionarios = @funcionarios.where(ativo: true)
      else
        @funcionarios = @funcionarios.where(ativo: false)
      end
    end
    
    @funcionario = Funcionario.new
    @grupos = Grupo.order :descricao
    respond_with @funcionarios
  end

  def atualizar
    @grupos = Grupo.all.order :descricao
  end

  def update
    if @funcionario.update(funcionario_params)
      redirect_to lista_de_funcionarios_path, notice: 'Funcionário atualizado!'
    else
      render :edit
    end
  end

  def create
    @funcionario = Funcionario.new(funcionario_params)

    if @funcionario.save
      redirect_to lista_de_funcionarios_path, notice: 'Cadastro realizado!'
    else
      render :new
    end
  end

  def destroy
    @funcionario.ativo = !@funcionario.ativo
    @funcionario.save!
    redirect_to lista_de_funcionarios_path, notice: "Funcionário atualizado!" 
  end

  private
    def funcionario_params
      params.require(:funcionario).permit(:nome, :email, :grupo_id, :chapa, :nome, :filial_id, :area_id)
    end

    def set_usuario
      @funcionario = Funcionario.find(params[:id])
    end

end
