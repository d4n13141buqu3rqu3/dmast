class DocumentoLocalizacoesController < ApplicationController
  before_action :set_documento_localizacao, only: [:show, :edit, :update, :destroy]
  before_filter :require_user
  def new
    @documento_localizacao = DocumentoLocalizacao.new
    @areas = Area.order(:descricao)
    @doc_id = params[:id]
  end

  def edit
  end

  def create
    @documento_localizacao = DocumentoLocalizacao.new(documento_localizacao_params)
    if @documento_localizacao.save
      hist = DocumentoHistorico.new
      hist.documento_id = @documento_localizacao.documento_id
      hist.historico = "Localização adicionada"
      hist.save!
      redirect_to "/atualizar-documento-#{@documento_localizacao.documento_id}", notice: 'Localização Salva com sucesso'
    else
      render :new
    end
  end

  def update
    if @documento_localizacao.update(documento_localizacao_params)
      redirect_to @documento_localizacao, notice: 'Documento localizacao was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @documento_localizacao.destroy
    redirect_to :back, notice: 'Localização excluída'
  end

  private
    def set_documento_localizacao
      @documento_localizacao = DocumentoLocalizacao.find(params[:id])
    end

    def documento_localizacao_params
      params.require(:documento_localizacao).permit(:documento_id, :area_id, :pasta_id, :numero_id, :localizacao_id)
    end
end
