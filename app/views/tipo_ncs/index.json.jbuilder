json.array!(@tipo_ncs) do |tipo_nc|
  json.extract! tipo_nc, :id, :sigla, :descricao
  json.url tipo_nc_url(tipo_nc, format: :json)
end
