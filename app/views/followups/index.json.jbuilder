json.array!(@followups) do |followup|
  json.extract! followup, :id, :auditoria_id, :descricao
  json.url followup_url(followup, format: :json)
end
