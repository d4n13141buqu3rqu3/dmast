json.array!(@tipo_ata) do |tipo_atum|
  json.extract! tipo_atum, :id
  json.url tipo_atum_url(tipo_atum, format: :json)
end
