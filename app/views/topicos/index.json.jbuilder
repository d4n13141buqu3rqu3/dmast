json.array!(@topicos) do |topico|
  json.extract! topico, :id, :descricao, :encerrado, :reuniao_id
  json.url topico_url(topico, format: :json)
end
