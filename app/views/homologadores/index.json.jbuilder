json.array!(@homologadores) do |homologador|
  json.extract! homologador, :id, :email
  json.url homologador_url(homologador, format: :json)
end
