json.array!(@rais) do |rai|
  json.extract! rai, :id, :auditoria_id, :objetivos, :resultado, :obs, :g_nconform, :usuario_id
  json.url rai_url(rai, format: :json)
end
