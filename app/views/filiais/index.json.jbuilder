json.array!(@filiais) do |filial|
  json.extract! filial, :id, :codigo, :nome
  json.url filial_url(filial, format: :json)
end
