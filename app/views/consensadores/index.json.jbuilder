json.array!(@consensadores) do |consensador|
  json.extract! consensador, :id, :email
  json.url consensador_url(consensador, format: :json)
end
