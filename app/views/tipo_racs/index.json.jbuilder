json.array!(@tipo_racs) do |tipo_rac|
  json.extract! tipo_rac, :id, :descricao
  json.url tipo_rac_url(tipo_rac, format: :json)
end
