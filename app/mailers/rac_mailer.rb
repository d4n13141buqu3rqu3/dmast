class RacMailer < ActionMailer::Base
  default from: "dmast.workflow@amcel.com.br"

  def rac_bloqueio(destinario, dados = {})
    @dados = dados
    mail(to: destinario, subject: "DMAST - Bloqueio de RAC")
  end
end