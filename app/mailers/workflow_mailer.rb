class WorkflowMailer < ActionMailer::Base
  default from: "dmast.workflow@amcel.com.br" 

  def msg(destinatario, dados = [])
  	@usuario = dados[0]
  	@link    = dados[1]
    mail(to: destinatario, subject: 'DMAST - WORKFLOW - CONSENSO')
  end

  def consenso_negado(destinatario, dados = [])
  	@usuario = dados[0]
  	@link    = dados[1]
  	@motivo  = dados[2]
  	mail(to: destinatario, subject: "DMAST - WORKFLOW - CONSENSO")
  end

  def consenso_aprovado(destinatario, titulo)
    @titulo = titulo
    mail(to: destinatario, subject: "DMAST - WORKFLOW - CONSENSO")
  end

  def enviar_homologacao(destinatario, dados = [])
    @usuario  = dados[0]
    @link     = dados[1]
    @motivo   = dados[2]
    mail(to: destinatario, subject: "DMAST - WORKFLOW - HOMOLOGAÇÃO")
  end

  def homologacao_negada(destinatario, dados = [])
    @usuario = dados[0]
    @link    = dados[1]
    @motivo  = dados[2]
    mail(to: destinatario, subject: "DMAST - WORKFLOW - HOMOLOGAÇÃO")
  end

  def homologacao_aprovada(destinatario, titulo)
    @titulo = titulo
    mail(to: destinatario, subject: "DMAST - WORKFLOW - HOMOLOGAÇÃO")
  end

  def homologacao_notificao_all(destinatario, titulo, link, sigla, tipo)
    @titulo = titulo
    mail(to: destinatario, subject: "DMAST - Novo documento homologado")
  end
  
  def enviar_followup destinatario, dados
    @dados = dados
    mail(to: destinatario, subject: "DMAST - FOLLOW-UP - AUDITORIA")
  end

end
