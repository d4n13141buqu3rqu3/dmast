class NotificacaoMailer < ActionMailer::Base
  default from: "daniel.albuquerque@amcel.com.br" #Não esquecer de alterar

  def msg(destinatario)
    mail(to: destinatario, subject: 'SISTEMA DMAST')
  end

  def rqa_sesmet(params)
    @dados = params
    mail(to: "sesmet@amcel.com.br", subject: "DMAST - Relato de quase acidente - RQA") if Rails.env.production?
  end

  def rqa_setor(params)
    @dados = params
    mail(to: params[:email_setor], subject: "DMAST - Relato de quase acidente - RQA") if params[:email_setor].present? && Rails.env.production?
  end

  def rac(destinatario, dados)
    @dados = dados
    mail(to: destinatario, subject: "DMAST - Notificação de RAC")
  end
end
