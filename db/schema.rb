# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150820145702) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "anexo_docs", force: true do |t|
    t.string   "anexo"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "documento_id"
    t.string   "descricao"
    t.integer  "user_id"
  end

  add_index "anexo_docs", ["user_id"], name: "index_anexo_docs_on_user_id", using: :btree

  create_table "anexo_reunioes", force: true do |t|
    t.integer  "reuniao_id"
    t.string   "descricao"
    t.string   "anexo"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "downloads",  default: 0
    t.integer  "user_id"
  end

  add_index "anexo_reunioes", ["reuniao_id"], name: "index_anexo_reunioes_on_reuniao_id", using: :btree
  add_index "anexo_reunioes", ["user_id"], name: "index_anexo_reunioes_on_user_id", using: :btree

  create_table "areas", force: true do |t|
    t.string   "descricao",  limit: 80,  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",      limit: 100
  end

  create_table "ata_reuniaos", force: true do |t|
    t.string   "assunto",       limit: 200, null: false
    t.datetime "data_reunicao",             null: false
    t.integer  "area_id",                   null: false
    t.integer  "tpAta_id",                  null: false
    t.text     "descricao",                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ata_reuniaos", ["area_id"], name: "index_ata_reuniaos_on_area_id", using: :btree
  add_index "ata_reuniaos", ["tpAta_id"], name: "index_ata_reuniaos_on_tpAta_id", using: :btree

  create_table "atividades", force: true do |t|
    t.string   "descricao",  limit: 80, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "auditoria_atividades", force: true do |t|
    t.integer  "auditoria_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "atividade_id"
  end

  add_index "auditoria_atividades", ["atividade_id"], name: "index_auditoria_atividades_on_atividade_id", using: :btree
  add_index "auditoria_atividades", ["auditoria_id"], name: "index_auditoria_atividades_on_auditoria_id", using: :btree

  create_table "auditorias", force: true do |t|
    t.integer  "planoauditoria_id"
    t.string   "status"
    t.integer  "area_id"
    t.datetime "data_prevista"
    t.integer  "auditor_lider_id"
    t.string   "duracao_prevista"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "hora_prevista"
    t.text     "legislacao"
    t.string   "numero"
    t.integer  "criado_por"
  end

  add_index "auditorias", ["area_id"], name: "index_auditorias_on_area_id", using: :btree
  add_index "auditorias", ["auditor_lider_id"], name: "index_auditorias_on_auditor_lider_id", using: :btree
  add_index "auditorias", ["planoauditoria_id"], name: "index_auditorias_on_planoauditoria_id", using: :btree

  create_table "auditorias_funcionarios", id: false, force: true do |t|
    t.integer "auditoria_id"
    t.integer "funcionario_id"
  end

  create_table "consensamentos", force: true do |t|
    t.integer  "documento_id"
    t.boolean  "consensado"
    t.text     "motivo_nao_consensamento"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.string   "email"
  end

  add_index "consensamentos", ["documento_id"], name: "index_consensamentos_on_documento_id", using: :btree

  create_table "demais_rac_historicos", force: true do |t|
    t.integer  "demais_rac_id",                 null: false
    t.string   "historico",         limit: 100, null: false
    t.datetime "NovaDt",                        null: false
    t.datetime "DtAnterior",                    null: false
    t.text     "justificativa",                 null: false
    t.string   "NovoResponsavel",   limit: 50,  null: false
    t.string   "AntigoResponsavel", limit: 50,  null: false
    t.integer  "user_id",                       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "demais_rac_historicos", ["demais_rac_id"], name: "index_demais_rac_historicos_on_demais_rac_id", using: :btree
  add_index "demais_rac_historicos", ["user_id"], name: "index_demais_rac_historicos_on_user_id", using: :btree

  create_table "demais_racs", force: true do |t|
    t.string   "status",                     limit: 25
    t.integer  "tipo_reclamacao_id"
    t.integer  "area_solicitante_id"
    t.integer  "area_responsavel_id"
    t.integer  "user_id"
    t.text     "causa_raiz"
    t.text     "desc_acao_corretiva"
    t.datetime "dt_acao_corretiva"
    t.datetime "nova_dt_prevista"
    t.text     "just_nova_dt"
    t.string   "responsavel_acao_corretiva", limit: 80
    t.text     "anal_critica"
    t.datetime "dt_real_encerramento"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "tipo_nc_id"
    t.text     "desc_nc"
    t.string   "solicitante",                limit: 50
    t.integer  "ano",                                                        null: false
    t.integer  "sequencia",                                                  null: false
    t.string   "NovoResponsavel",            limit: 50
    t.datetime "dt_criacao",                                                 null: false
    t.string   "status_correcao",            limit: 30, default: "Pendente", null: false
  end

  create_table "documento_complementars", force: true do |t|
    t.integer  "doc_pai_id"
    t.integer  "doc_filho_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "documento_historicos", force: true do |t|
    t.integer  "documento_id"
    t.text     "historico"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "documento_historicos", ["documento_id"], name: "index_documento_historicos_on_documento_id", using: :btree

  create_table "documento_localizacoes", force: true do |t|
    t.integer  "documento_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "area_id"
    t.integer  "numero_id"
    t.integer  "localizacao_id"
    t.integer  "pasta_id"
  end

  add_index "documento_localizacoes", ["area_id"], name: "index_documento_localizacoes_on_area_id", using: :btree
  add_index "documento_localizacoes", ["documento_id"], name: "index_documento_localizacoes_on_documento_id", using: :btree
  add_index "documento_localizacoes", ["localizacao_id"], name: "index_documento_localizacoes_on_localizacao_id", using: :btree
  add_index "documento_localizacoes", ["numero_id"], name: "index_documento_localizacoes_on_numero_id", using: :btree
  add_index "documento_localizacoes", ["pasta_id"], name: "index_documento_localizacoes_on_pasta_id", using: :btree

  create_table "documentos", force: true do |t|
    t.string   "titulo",               limit: 80
    t.integer  "tipoDoc_id"
    t.string   "status",               limit: 30
    t.text     "texto_doc"
    t.integer  "dias_consenso"
    t.integer  "dias_homologar"
    t.datetime "dt_envio_consenso"
    t.datetime "dt_envio_homologacao"
    t.string   "sigla",                limit: 30
    t.string   "versao",               limit: 50
    t.string   "versao_anterior",      limit: 50
    t.integer  "id_doc_anterior"
    t.string   "link_versao_anterior", limit: 200
    t.datetime "created_at"
    t.datetime "dt_consenso"
    t.datetime "dt_homologacao"
    t.integer  "user_id"
    t.string   "elaborador",           limit: 50,                  null: false
    t.boolean  "excluido",                         default: false, null: false
    t.string   "motivo_exclusao",      limit: 50
    t.datetime "deleted_at"
    t.integer  "grp_consenso_id"
    t.integer  "grp_homologador_id"
    t.integer  "doc_pai_id"
  end

  add_index "documentos", ["grp_consenso_id"], name: "index_documentos_on_grp_consenso_id", using: :btree
  add_index "documentos", ["grp_homologador_id"], name: "index_documentos_on_grp_homologador_id", using: :btree
  add_index "documentos", ["tipoDoc_id"], name: "index_documentos_on_tipoDoc_id", using: :btree

  create_table "documentos_areas", id: false, force: true do |t|
    t.integer "documento_id"
    t.integer "area_id"
  end

  create_table "documentos_docscomplementares", id: false, force: true do |t|
    t.integer "doc_pai_id"
    t.integer "doc_filho_id"
  end

  create_table "filiais", force: true do |t|
    t.string "codigo", limit: 5,  null: false
    t.string "nome",   limit: 25, null: false
  end

  create_table "funcionarios", force: true do |t|
    t.string   "chapa",      limit: 10,                 null: false
    t.string   "nome",       limit: 80,                 null: false
    t.boolean  "ativo",                  default: true, null: false
    t.string   "email",      limit: 200
    t.integer  "filial_id",                             null: false
    t.integer  "area_id"
    t.integer  "grupo_id",                              null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "funcionarios", ["area_id"], name: "index_funcionarios_on_area_id", using: :btree
  add_index "funcionarios", ["filial_id"], name: "index_funcionarios_on_filial_id", using: :btree
  add_index "funcionarios", ["grupo_id"], name: "index_funcionarios_on_grupo_id", using: :btree

  create_table "grp_consensadores", force: true do |t|
    t.string "descricao", limit: 100, null: false
  end

  create_table "grp_homologadores", force: true do |t|
    t.string "descricao", limit: 100, null: false
  end

  create_table "grpconsensador_funcionarios", id: false, force: true do |t|
    t.integer "funcionario_id"
    t.integer "grp_consensador_id"
  end

  create_table "grphomologadores_funcionarios", id: false, force: true do |t|
    t.integer "funcionario_id"
    t.integer "grp_homologador_id"
  end

  create_table "grupos", force: true do |t|
    t.string   "descricao",  limit: 50, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "helpers", force: true do |t|
    t.string   "titulo",     limit: 40
    t.text     "corpo"
    t.string   "versao",     limit: 5
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "helpers", ["user_id"], name: "index_helpers_on_user_id", using: :btree

  create_table "homologacoes", force: true do |t|
    t.integer  "documento_id"
    t.boolean  "homologado"
    t.text     "motivo_nao_homolog"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.string   "email"
  end

  add_index "homologacoes", ["documento_id"], name: "index_homologacoes_on_documento_id", using: :btree

  create_table "images", force: true do |t|
    t.string   "alt",               default: ""
    t.string   "hint",              default: ""
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locais", force: true do |t|
    t.string   "descricao"
    t.boolean  "disponivel"
    t.string   "motivo_indisp"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "localizacoes", force: true do |t|
    t.integer "numero_id"
    t.string  "descricao", limit: 100, null: false
  end

  add_index "localizacoes", ["numero_id"], name: "index_localizacoes_on_numero_id", using: :btree

  create_table "notificacoes", force: true do |t|
    t.string   "descricao"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
    t.boolean  "visto"
    t.string   "notiemail",  limit: 100
  end

  add_index "notificacoes", ["user_id"], name: "index_notificacoes_on_user_id", using: :btree

  create_table "numeros", force: true do |t|
    t.integer "pasta_id"
    t.string  "descricao", limit: 50, null: false
  end

  add_index "numeros", ["pasta_id"], name: "index_numeros_on_pasta_id", using: :btree

  create_table "pastas", force: true do |t|
    t.integer "area_id"
    t.string  "descricao", limit: 50, null: false
  end

  add_index "pastas", ["area_id"], name: "index_pastas_on_area_id", using: :btree

  create_table "planos_funcionarios", id: false, force: true do |t|
    t.integer "planoauditoria_id"
    t.integer "funcionario_id"
  end

  create_table "planosauditorias", force: true do |t|
    t.string   "nome",        limit: 100,                 null: false
    t.boolean  "encerrado",               default: false
    t.integer  "prazo",                                   null: false
    t.string   "ano"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "data_inicio"
    t.datetime "data_fim"
  end

  create_table "rac_followups", force: true do |t|
    t.integer  "rac_id"
    t.integer  "user_id"
    t.datetime "dt_followup"
    t.string   "hora",        limit: 10
    t.string   "responsavel", limit: 100
    t.boolean  "finalizado",              default: false
    t.text     "inf_inicial"
    t.text     "inf_final"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rac_followups", ["rac_id"], name: "index_rac_followups_on_rac_id", using: :btree
  add_index "rac_followups", ["user_id"], name: "index_rac_followups_on_user_id", using: :btree

  create_table "rac_historicos", force: true do |t|
    t.integer  "rac_id"
    t.string   "historico"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "NovaDt"
    t.datetime "DtAnterior"
    t.text     "justificativa"
    t.string   "NovoResponsavel"
    t.string   "AntigoResponsavel"
    t.integer  "user_id"
  end

  add_index "rac_historicos", ["rac_id"], name: "index_rac_historicos_on_rac_id", using: :btree
  add_index "rac_historicos", ["user_id"], name: "index_rac_historicos_on_user_id", using: :btree

  create_table "rac_nao_conformidades", force: true do |t|
    t.integer  "rac_id"
    t.integer  "tipo_nc_id"
    t.text     "descricao"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rac_nao_conformidades", ["rac_id"], name: "index_rac_nao_conformidades_on_rac_id", using: :btree
  add_index "rac_nao_conformidades", ["tipo_nc_id"], name: "index_rac_nao_conformidades_on_tipo_nc_id", using: :btree

  create_table "racs", force: true do |t|
    t.integer  "auditoria_id"
    t.string   "status",                limit: 20
    t.string   "responsavel",           limit: 60
    t.text     "analise_causa"
    t.string   "status_correcao",       limit: 25
    t.text     "desc_correacao"
    t.datetime "dt_prevista_correacao"
    t.datetime "nova_dt_prevista"
    t.text     "justificativa"
    t.string   "responsavel_correcao",  limit: 100
    t.text     "analise_eficacia"
    t.datetime "dt_real_encerramento"
    t.datetime "dt_real_execucao"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "tipo_nc_id"
    t.text     "desc_nc"
    t.string   "NovoResponsavel"
    t.datetime "dt_criacao",                                        null: false
    t.string   "solicitante",           limit: 100,                 null: false
    t.boolean  "notifica_responsavel",              default: false, null: false
    t.integer  "ano",                                               null: false
    t.integer  "sequencia",                                         null: false
    t.integer  "user_id"
    t.boolean  "bloqueado",                         default: false
  end

  add_index "racs", ["auditoria_id"], name: "index_racs_on_auditoria_id", using: :btree
  add_index "racs", ["user_id"], name: "index_racs_on_user_id", using: :btree

  create_table "racs_participantes", id: false, force: true do |t|
    t.integer "rac_id"
    t.integer "usuario_id"
  end

  create_table "rai_followups", force: true do |t|
    t.integer  "rai_id"
    t.integer  "user_id"
    t.datetime "dt_followup"
    t.string   "hora",        limit: 10
    t.string   "responsavel", limit: 100
    t.boolean  "finalizado",              default: false
    t.text     "inf_inicial"
    t.text     "inf_final"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rai_followups", ["rai_id"], name: "index_rai_followups_on_rai_id", using: :btree
  add_index "rai_followups", ["user_id"], name: "index_rai_followups_on_user_id", using: :btree

  create_table "rais", force: true do |t|
    t.integer  "auditoria_id"
    t.text     "objetivos"
    t.text     "resultado"
    t.text     "obs"
    t.boolean  "g_nconform"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "lider_id"
    t.integer  "user_id"
    t.text     "docs_ref"
  end

  add_index "rais", ["auditoria_id"], name: "index_rais_on_auditoria_id", using: :btree
  add_index "rais", ["user_id"], name: "index_rais_on_user_id", using: :btree

  create_table "rais_participantes", id: false, force: true do |t|
    t.integer "rai_id"
    t.integer "funcionario_id"
  end

  create_table "rap_followups", force: true do |t|
    t.integer  "rap_id"
    t.integer  "user_id"
    t.datetime "dt_followup"
    t.string   "hora",        limit: 10
    t.string   "responsavel", limit: 100
    t.boolean  "finalizado",              default: false
    t.text     "inf_inicial"
    t.text     "inf_final"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rap_followups", ["rap_id"], name: "index_rap_followups_on_rap_id", using: :btree
  add_index "rap_followups", ["user_id"], name: "index_rap_followups_on_user_id", using: :btree

  create_table "rap_historicos", force: true do |t|
    t.integer  "rap_id"
    t.string   "historico"
    t.datetime "dtAntiga"
    t.datetime "dtNova"
    t.string   "justificativa"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rap_historicos", ["rap_id"], name: "index_rap_historicos_on_rap_id", using: :btree

  create_table "raps", force: true do |t|
    t.string   "status"
    t.integer  "tipo_reclamacao_id"
    t.integer  "area_responsavel_id"
    t.integer  "area_solicitante_id"
    t.string   "solicitante",         limit: 60
    t.integer  "tipo_nc_id"
    t.text     "desc_nc"
    t.string   "status_acao",         limit: 25
    t.text     "desc_acao"
    t.datetime "dt_prevista"
    t.datetime "nova_dt"
    t.text     "justificativa"
    t.text     "analise_critica"
    t.datetime "dt_encerramento"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "inf"
    t.integer  "sequencia"
    t.integer  "ano"
    t.datetime "dt_criacao"
    t.integer  "user_id"
  end

  add_index "raps", ["area_responsavel_id"], name: "index_raps_on_area_responsavel_id", using: :btree
  add_index "raps", ["area_solicitante_id"], name: "index_raps_on_area_solicitante_id", using: :btree
  add_index "raps", ["tipo_nc_id"], name: "index_raps_on_tipo_nc_id", using: :btree
  add_index "raps", ["tipo_reclamacao_id"], name: "index_raps_on_tipo_reclamacao_id", using: :btree
  add_index "raps", ["user_id"], name: "index_raps_on_user_id", using: :btree

  create_table "raps_participantes", id: false, force: true do |t|
    t.integer "rap_id"
    t.integer "usuario_id"
  end

  create_table "reunioes", force: true do |t|
    t.string   "assunto",       limit: 150
    t.datetime "data_reuniao",              null: false
    t.integer  "area_id",                   null: false
    t.integer  "TipoAta_id",                null: false
    t.text     "descricao"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.string   "local_reuniao", limit: 100, null: false
  end

  add_index "reunioes", ["TipoAta_id"], name: "index_reunioes_on_TipoAta_id", using: :btree
  add_index "reunioes", ["area_id"], name: "index_reunioes_on_area_id", using: :btree

  create_table "reunioes_funcionarios", id: false, force: true do |t|
    t.integer "reuniao_id"
    t.integer "funcionario_id"
  end

  create_table "rqas", force: true do |t|
    t.text     "descricao",                              null: false
    t.text     "proposta"
    t.string   "responsavel", limit: 60,                 null: false
    t.string   "prazo",       limit: 30
    t.datetime "dt",                                     null: false
    t.integer  "area_id",                                null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "filial"
    t.string   "categoria"
    t.boolean  "AI",                     default: false
    t.boolean  "CI",                     default: false
    t.integer  "user_id"
  end

  add_index "rqas", ["area_id"], name: "index_rqas_on_area_id", using: :btree

  create_table "tipo_atas", force: true do |t|
    t.string   "descricao",  limit: 80, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipo_docs", force: true do |t|
    t.string   "descricao",  limit: 50
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipo_ncs", force: true do |t|
    t.string   "sigla",      limit: 10
    t.string   "descricao",  limit: 80
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipo_reclamacoes", force: true do |t|
    t.string   "descricao",  limit: 50
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "topicos", force: true do |t|
    t.string   "descricao"
    t.boolean  "encerrado"
    t.integer  "reuniao_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "topicos", ["reuniao_id"], name: "index_topicos_on_reuniao_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.string   "photo"
    t.string   "email"
    t.datetime "oauth_expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
