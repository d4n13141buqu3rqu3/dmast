class CreateRapHistoricos < ActiveRecord::Migration
  def change
    create_table :rap_historicos do |t|
      t.belongs_to :rap, index: true
      t.string :historico
      t.datetime :dtAntiga
      t.datetime :dtNova
      t.string :justificativa

      t.timestamps
    end
  end
end
