class AjusteReuniaoLoc < ActiveRecord::Migration
  def change
    remove_column :reunioes, :local
    add_column :reunioes, :local_reuniao, :string, limit: 100, null: false
  end
end
