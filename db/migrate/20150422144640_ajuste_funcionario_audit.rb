class AjusteFuncionarioAudit < ActiveRecord::Migration
  def change
    drop_table :auditorias_funcionarios

    create_table :auditorias_funcionarios, :id => false do |t|
      t.integer :auditoria_id
      t.integer :funcionario_id
    end
  end
end
