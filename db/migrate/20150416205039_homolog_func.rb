class HomologFunc < ActiveRecord::Migration
  def change
    create_table :grphomologadores_funcionarios, :id => false do |t|
      t.integer :funcionario_id
      t.integer :grp_homologador_id
    end
  end
end
