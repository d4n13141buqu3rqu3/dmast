class AjustDocuemtLocal2 < ActiveRecord::Migration
  def change
  	remove_column(:documento_localizacoes, :pasta)

  	add_reference(:documento_localizacoes, :pasta, index: true)
  end
end
