class DocsDocscomplementaresJointable < ActiveRecord::Migration
  def change
    create_table :documentos_docscomplementares, :id => false do |t|
      t.integer :doc_pai_id
      t.integer :doc_filho_id
    end
  end
end
