class Removenamefromdocs < ActiveRecord::Migration
  def change
    remove_column :documentos, :nome
    remove_column :documentos, :modificado_por_id
    remove_column :documentos, :nome_original
  end
end
