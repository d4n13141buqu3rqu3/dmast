class CreateRacNaoConformidades < ActiveRecord::Migration
  def change
    create_table :rac_nao_conformidades do |t|
      t.belongs_to :rac, index: true
      t.belongs_to :tipo_nc, index: true
      t.text :descricao

      t.timestamps
    end
  end
end
