class CreateFollowups < ActiveRecord::Migration
  def change
    create_table :followups do |t|
      t.belongs_to :auditoria, index: true
      t.string :descricao

      t.timestamps
    end
  end
end
