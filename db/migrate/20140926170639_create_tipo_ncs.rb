class CreateTipoNcs < ActiveRecord::Migration
  def change
    create_table :tipo_ncs do |t|
      t.string :sigla, limit: 10
      t.string :descricao, limit: 80

      t.timestamps
    end
  end
end
