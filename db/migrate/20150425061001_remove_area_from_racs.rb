class RemoveAreaFromRacs < ActiveRecord::Migration
  def change
    remove_column :racs, :area_id
  end
end
