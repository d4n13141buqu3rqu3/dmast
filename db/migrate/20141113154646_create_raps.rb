class CreateRaps < ActiveRecord::Migration
  def change
    create_table :raps do |t|
      t.string :status
      t.belongs_to :tipo_reclamacao, index: true
      t.belongs_to :area_responsavel, index: true
      t.belongs_to :area_solicitante, index: true
      t.string :solicitante, limit: 60
      t.belongs_to :tipo_nc, index: true
      t.text :desc_nc
      t.string :status_acao, limit: 20
      t.text :desc_acao
      t.text :consequencia
      t.datetime :dt_prevista
      t.datetime :nova_dt
      t.text :justificativa
      t.text :analise_critica
      t.datetime :dt_encerramento
      t.belongs_to :usuario, index: true

      t.timestamps
    end
  end
end
