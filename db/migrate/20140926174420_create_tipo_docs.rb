class CreateTipoDocs < ActiveRecord::Migration
  def change
    create_table :tipo_docs do |t|
      t.string :descricao, limit: 50

      t.timestamps
    end
  end
end
