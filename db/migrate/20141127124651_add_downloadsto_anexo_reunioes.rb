class AddDownloadstoAnexoReunioes < ActiveRecord::Migration
  def change
    add_column :anexo_reunioes, :downloads, :integer, default: 0
  end
end
