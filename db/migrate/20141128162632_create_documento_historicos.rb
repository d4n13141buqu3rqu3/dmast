class CreateDocumentoHistoricos < ActiveRecord::Migration
  def change
    create_table :documento_historicos do |t|
      t.belongs_to :documento, index: true
      t.string :historico

      t.timestamps
    end
  end
end
