class RemoveTipoRac < ActiveRecord::Migration
  def change
    remove_column :racs, :tipo_rac_id

    drop_table :tipo_racs
  end
end
