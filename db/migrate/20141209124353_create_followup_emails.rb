class CreateFollowupEmails < ActiveRecord::Migration
  def change
    create_table :followup_emails do |t|
      t.belongs_to :followup, index: true
      t.string :email
      t.boolean :enviado

      t.timestamps
    end
  end
end
