class AddNovoResponsavelToRacs < ActiveRecord::Migration
  def change
    add_column :rac_historicos, :NovoResponsavel, :string
    add_column :rac_historicos, :AntigoResponsavel, :string
  end
end
