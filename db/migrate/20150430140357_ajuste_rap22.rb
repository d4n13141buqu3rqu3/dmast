class AjusteRap22 < ActiveRecord::Migration
  def change
    change_column :raps, :status_acao, :string, :limit => 25
    change_column :racs, :status_correcao, :string, :limit => 25
    change_column :demais_racs, :status_correcao, :string, :limit => 25
  end
end
