class AddStatusCorrecaoToDemaisRacs < ActiveRecord::Migration
  def change
    add_column :demais_racs, :status_correcao, :string, limit: 15
  end
end
