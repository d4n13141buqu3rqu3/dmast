class CreateRaiFollowups < ActiveRecord::Migration
  def change
    create_table :rai_followups do |t|
      t.references :rai, index: true
      t.references :user, index: true
      t.string :codigo, limit: 20, index: true
      t.datetime :dt_followup
      t.string :hora, limit: 10
      t.string :responsavel, limit: 100
      t.boolean :finalizado, default: false
      t.text :inf_inicial
      t.text :inf_final
      t.timestamps

      t.timestamps
    end
  end
end
