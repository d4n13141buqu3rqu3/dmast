class CreateAnexoDocs < ActiveRecord::Migration
  def change
    create_table :anexo_docs do |t|
      t.integer :usuario
      t.string :anexo
      t.timestamps
    end
  end
end
