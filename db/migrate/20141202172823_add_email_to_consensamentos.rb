class AddEmailToConsensamentos < ActiveRecord::Migration
  def change
    add_column :consensamentos, :email, :string
  end
end
