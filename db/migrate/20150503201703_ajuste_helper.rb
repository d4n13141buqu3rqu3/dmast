class AjusteHelper < ActiveRecord::Migration
  def change
    remove_column :helpers, :User_id
    add_reference :helpers, :user, index: true
  end
end
