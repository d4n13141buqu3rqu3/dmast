class AjustefuncionarioGrupo < ActiveRecord::Migration
  def change
    #drop_table :funcionarios

    create_table :grupos_funcionarios, :id => false do |t|
      t.integer :funcionario_id
      t.integer :grupo_id
    end
  end
end
