class ConsensadorFunc < ActiveRecord::Migration
  def change
    create_table :grpconsensador_funcionarios, :id => false do |t|
      t.integer :funcionario_id
      t.integer :grp_consensador_id
    end
  end
end
