class CreateRacs < ActiveRecord::Migration
  def change
    create_table :racs do |t|
      t.belongs_to :auditoria, index: true
      t.belongs_to :usuario, index: true
      t.string :status, limit: 20
      t.belongs_to :tipo_rac, index: true
      t.belongs_to :area, index: true
      t.string :responsavel, limit: 60
      t.text :analise_causa
      t.string :status_correcao, limit: 60
      t.text :desc_correacao
      t.datetime :dt_prevista_correacao
      t.datetime :nova_dt_prevista
      t.text :justificativa
      t.string :responsavel_correcao, limit: 100
      t.text :analise_eficacia
      t.datetime :dt_real_encerramento
      t.datetime :dt_real_execucao

      t.timestamps
    end
  end
end
