class CreateAtaReuniaos < ActiveRecord::Migration
  def change
    create_table :ata_reuniaos do |t|
      t.string :assunto, limit: 200, null: false
      t.datetime :data_reunicao, null: false
      t.belongs_to :area, index: true, null: false
      t.belongs_to :tpAta, index: true, null: false
      t.text :descricao, null: false

      t.timestamps
    end
  end
end
