class AddDefaultValueToStatusCorreacao < ActiveRecord::Migration
  def change
    change_column(:demais_racs, :status_correcao, :string, null: false, limit: 30, default: "Pendente")
  end
end
