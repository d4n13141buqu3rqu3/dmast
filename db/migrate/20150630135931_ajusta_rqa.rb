class AjustaRqa < ActiveRecord::Migration
  def change
    remove_column(:rqas, :usuario_id)
    add_reference(:rqas, :user)
  end
end
