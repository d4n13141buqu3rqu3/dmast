class CreateHelpers < ActiveRecord::Migration
  def change
    create_table :helpers do |t|
      t.string :titulo, limit: 40
      t.text :corpo
      t.string :versao, limit: 5
      t.belongs_to :User, index: true

      t.timestamps
    end
  end
end
