class CreateTopicos < ActiveRecord::Migration
  def change
    create_table :topicos do |t|
      t.string :descricao
      t.boolean :encerrado
      t.belongs_to :reuniao, index: true

      t.timestamps
    end
  end
end
