class CreateAtividades < ActiveRecord::Migration
  def change
    create_table :atividades do |t|
      t.string :descricao, null: false, limit: 80

      t.timestamps
    end
  end
end
