class CreateAnexoReunioes < ActiveRecord::Migration
  def change
    create_table :anexo_reunioes do |t|
      t.belongs_to :reuniao, index: true
      t.string :descricao
      t.string :anexo
      t.timestamps
    end
  end
end
