class AjusteLocalizacaoesDocumentos < ActiveRecord::Migration
  def change
  	change_column(:localizacoes, :descricao, :string, limit: 100, null: false)
  end
end
