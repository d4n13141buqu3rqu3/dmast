class CreateTipoReclamacaos < ActiveRecord::Migration
  def change
    create_table :tipo_reclamacoes do |t|
      t.string :descricao, limit: 50

      t.timestamps
    end
  end
end
