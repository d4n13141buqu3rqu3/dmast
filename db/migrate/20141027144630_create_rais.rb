class CreateRais < ActiveRecord::Migration
  def change
    create_table :rais do |t|
      t.belongs_to :auditoria, index: true
      t.text :objetivos
      t.text :resultado
      t.text :obs
      t.boolean :g_nconform
      t.belongs_to :usuario, index: true

      t.timestamps
    end
  end
end
