class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|
      t.string :descricao, limit: 80, null: false	
      t.timestamps
    end
  end
end
