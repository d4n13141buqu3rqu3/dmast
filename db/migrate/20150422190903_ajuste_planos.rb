class AjustePlanos < ActiveRecord::Migration
  def change
    drop_table :planos_usuarios

    create_table :planos_funcionarios, :id => false do |t|
      t.integer :plano_id
      t.integer :funcionario_id
    end
  end
end
