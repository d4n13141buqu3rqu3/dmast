class CreateLocalizacoes < ActiveRecord::Migration
  def change
    create_table :localizacoes do |t|
      t.belongs_to :numero, index: true
      t.string :descricao, limit: 50, null: false
    end
  end
end
