class CreateFuncionarios < ActiveRecord::Migration
  def change
    create_table :funcionarios do |t|
      t.string :chapa, null: false, limit: 10
      t.string :nome, null: false, limit: 80
      t.boolean :ativo, null: false, default: true
      t.string :email, null: false, limit: 150
      t.belongs_to :filial, index: true, null: false
      t.belongs_to :area, index: true, null: false
      t.belongs_to :grupo, index: true, null: false

      t.timestamps
    end
  end
end
