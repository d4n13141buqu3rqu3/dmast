class CreateReunioes < ActiveRecord::Migration
  def change
    create_table :reunioes do |t|
      t.string :assunto, limit: 150
      t.datetime :data_reuniao, null: false
      t.belongs_to :area, index: true, null: false
      t.belongs_to :TipoAta, index: true, null: false
      t.text :descricao

      t.timestamps
    end
  end
end
