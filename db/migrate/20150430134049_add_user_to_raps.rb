class AddUserToRaps < ActiveRecord::Migration
  def change
    add_reference :raps, :user, index: true
  end
end
