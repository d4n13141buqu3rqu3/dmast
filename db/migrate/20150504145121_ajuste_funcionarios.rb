class AjusteFuncionarios < ActiveRecord::Migration
  def change
    change_column(:funcionarios, :area_id, :integer, null: true)
  end
end
