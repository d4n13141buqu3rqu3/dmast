class AddUsuarioIdToAnexoDocs < ActiveRecord::Migration
  def change
    add_reference :anexo_docs, :usuario_id, index: true
  end
end
