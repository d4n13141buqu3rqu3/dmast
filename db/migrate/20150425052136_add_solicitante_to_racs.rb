class AddSolicitanteToRacs < ActiveRecord::Migration
  def change
    add_column :racs, :solicitante, :string, limit: 100, null: false
  end
end
