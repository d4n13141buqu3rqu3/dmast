class AjusteAtividades < ActiveRecord::Migration
  def change
    remove_column :auditoria_atividades, :descricao

    add_reference :auditoria_atividades, :atividade, index: true
  end
end
