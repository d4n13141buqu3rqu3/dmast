class CreatePlanosauditorias < ActiveRecord::Migration
  def change
    create_table :planosauditorias do |t|
      t.string :nome, limit: 100, null: false
      t.boolean :encerrado, default: false
      t.integer :prazo, null: false
      t.string :ano

      t.timestamps
    end
  end
end
