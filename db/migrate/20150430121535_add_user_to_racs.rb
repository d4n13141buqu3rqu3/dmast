class AddUserToRacs < ActiveRecord::Migration
  def change
    add_reference :racs, :user, index: true
  end
end
