class Ajustedocumentosfuncionarios < ActiveRecord::Migration
  def change
    remove_column :documentos, :updated_at
    remove_column :documentos, :usuario_id

    add_column :documentos, :user_id, :integer, index: true
    add_column :documentos, :elaborador, :string, limit: 50, null: false
  end
end
