class CreateGrupos < ActiveRecord::Migration
  def change
    create_table :grupos do |t|
      t.string :descricao, limit: 50, null: false

      t.timestamps
    end
  end
end
