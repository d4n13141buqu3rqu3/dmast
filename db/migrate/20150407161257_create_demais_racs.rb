class CreateDemaisRacs < ActiveRecord::Migration
  def change
    create_table :demais_racs do |t|
      t.string :status, limit: 25
      t.belongs_to :tipo_reclamacao
      t.belongs_to :area_solicitante
      t.belongs_to :area_responsavel
      t.belongs_to :user
      t.text :causa_raiz
      t.text :desc_acao_corretiva
      t.datetime :dt_acao_corretiva
      t.datetime :nova_dt_prevista
      t.text :just_nova_dt
      t.string :responsavel_acao_corretiva, limit: 80
      t.text :anal_critica
      t.datetime :dt_real_encerramento

      t.timestamps
    end
  end


end
