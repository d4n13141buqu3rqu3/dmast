class CreateNumeros < ActiveRecord::Migration
  def change
    create_table :numeros do |t|
      t.belongs_to :pasta, index: true
      t.string :descricao, limit: 50, null: false
    end
  end
end
