class CreateLocais < ActiveRecord::Migration
  def change
    create_table :locais do |t|
      t.string :descricao
      t.boolean :disponivel
      t.string :motivo_indisp

      t.timestamps
    end
  end
end
