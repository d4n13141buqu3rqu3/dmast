class AddNotificaResponsalvelToRacs < ActiveRecord::Migration
  def change
    add_column :racs, :notifica_responsavel, :boolean, null: false, default: false
  end
end
