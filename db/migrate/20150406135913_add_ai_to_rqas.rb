class AddAiToRqas < ActiveRecord::Migration
  def change
    add_column :rqas, :AI, :boolean, default: false
    add_column :rqas, :CI, :boolean, default: false
  end
end
