class Ajustedocsdelete < ActiveRecord::Migration
  def change
    add_column :documentos, :excluido, :boolean, null: false, default: false
    add_column :documentos, :motivo_exclusao, :string, limit: 50
  end
end
