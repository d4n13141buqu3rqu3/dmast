class AjusteAnexoDocs < ActiveRecord::Migration
  def change
  	remove_column :anexo_docs, :usuario
  	remove_column :anexo_docs, :usuario_id
  	remove_column :anexo_docs, :usuario_id_id

  	add_reference :anexo_docs, :user, index: true
  end
end
