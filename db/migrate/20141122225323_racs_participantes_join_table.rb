class RacsParticipantesJoinTable < ActiveRecord::Migration
  def change
    create_table :racs_participantes, :id => false do |t|
      t.integer :rac_id
      t.integer :usuario_id
    end
  end
end
