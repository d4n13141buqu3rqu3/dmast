class AddHoraPrevistaToAuditorias < ActiveRecord::Migration
  def change
    add_column :auditorias, :hora_prevista, :string
  end
end
