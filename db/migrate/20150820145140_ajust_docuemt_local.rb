class AjustDocuemtLocal < ActiveRecord::Migration
  def change
  	remove_column(:documento_localizacoes, :area)
  	remove_column(:documento_localizacoes, :numero)
  	remove_column(:documento_localizacoes, :localizacao)

  	add_reference(:documento_localizacoes, :area, index: true)
  	add_reference(:documento_localizacoes, :numero, index: true)
  	add_reference(:documento_localizacoes, :localizacao, index: true)
  end
end
