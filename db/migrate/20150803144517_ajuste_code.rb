class AjusteCode < ActiveRecord::Migration
  def change
    remove_column(:rai_followups, :codigo)
    remove_column(:rac_followups, :codigo)
    remove_column(:rap_followups, :codigo)
  end
end
