class AddDtConsensoToDocumentos < ActiveRecord::Migration
  def change
    add_column :documentos, :dt_consenso, :datetime
    add_column :documentos, :dt_homologacao, :datetime
  end
end
