class AjusteRacCodigo < ActiveRecord::Migration
  def change
    remove_column :racs, :codigo_rac

    add_column :racs, :ano, :integer, null: false
    add_column :racs, :sequencia, :integer, null: false

  end
end
