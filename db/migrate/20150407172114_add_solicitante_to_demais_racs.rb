class AddSolicitanteToDemaisRacs < ActiveRecord::Migration
  def change
    add_column :demais_racs, :solicitante, :string, limit: 50
  end
end
