class CreateRacFollowups < ActiveRecord::Migration
  def change
    create_table :rac_followups do |t|
      t.references :rac, index: true
      t.references :user, index: true
      t.string :codigo, limit: 20, index: true
      t.datetime :dt_followup
      t.string :hora, limit: 10
      t.string :responsavel, limit: 100
      t.boolean :finalizado, default: false
      t.text :inf_inicial
      t.text :inf_final
      t.timestamps
    end
  end
end
