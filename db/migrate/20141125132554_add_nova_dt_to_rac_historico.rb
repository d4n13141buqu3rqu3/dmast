class AddNovaDtToRacHistorico < ActiveRecord::Migration
  def change
    add_column :rac_historicos, :NovaDt, :datetime
    add_column :rac_historicos, :DtAnterior, :datetime
    add_column :rac_historicos, :justificativa, :text
    add_column :rac_historicos, :usuario_id, :integer
  end
end
