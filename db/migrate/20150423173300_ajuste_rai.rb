class AjusteRai < ActiveRecord::Migration
  def change
    drop_table :rais_participantes

    create_table :rais_participantes, :id => false do |t|
      t.integer :rai_id
      t.integer :funcionario_id
    end

    change_table :rais do |t|
      t.remove :usuario_id, :obs2

      t.belongs_to :user, index: true


    end
  end
end
