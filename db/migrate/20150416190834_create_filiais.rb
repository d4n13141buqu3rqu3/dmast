class CreateFiliais < ActiveRecord::Migration
  def change
    create_table :filiais do |t|
      t.string :codigo, null: false, limit: 5, index: true
      t.string :nome, null: false, limit: 25, index: true
    end
  end
end
