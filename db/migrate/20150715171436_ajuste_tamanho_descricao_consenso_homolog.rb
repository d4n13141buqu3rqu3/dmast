class AjusteTamanhoDescricaoConsensoHomolog < ActiveRecord::Migration
  def change
    change_column(:grp_consensadores, :descricao, :string, limit: 100, null: false)
    change_column(:grp_homologadores, :descricao, :string, limit: 100, null: false)
  end
end
