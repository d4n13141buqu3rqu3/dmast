class AddStatusToConsensamentos < ActiveRecord::Migration
  def change
    add_column :consensamentos, :status, :string
  end
end
