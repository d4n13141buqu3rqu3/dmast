class CreateAuditoriaAtividades < ActiveRecord::Migration
  def change
    create_table :auditoria_atividades do |t|
      t.belongs_to :auditoria, index: true
      t.string :descricao

      t.timestamps
    end
  end
end
