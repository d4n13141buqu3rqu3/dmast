class AjusteDemaisRacs < ActiveRecord::Migration
  def change
    add_column :demais_racs, :ano, :integer, null: false
    add_column :demais_racs, :sequencia, :integer, null: false
    add_column :demais_racs, :notifica_responsavel, :boolean, null: false, default: false
    add_column :demais_racs, :NovoResponsavel, :string, limit: 50
  end
end
