class CreatePastas < ActiveRecord::Migration
  def change
    create_table :pastas do |t|
      t.belongs_to :area, index: true
      t.string :descricao, limit: 50, null: false
    end
  end
end
