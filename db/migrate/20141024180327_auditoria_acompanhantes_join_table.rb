class AuditoriaAcompanhantesJoinTable < ActiveRecord::Migration
  def change
  	create_table :auditorias_usuarios, :id => false do |t|
    	t.integer :auditoria_id
    	t.integer :user_id
    end
  end
end
