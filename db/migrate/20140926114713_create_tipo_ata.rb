class CreateTipoAta < ActiveRecord::Migration
  def change
    create_table :tipo_atas do |t|
      t.string :descricao, limit: 80, null: false
      t.timestamps
    end
  end
end
