class CreateDemaisRacHistoricos < ActiveRecord::Migration
  def change
    create_table :demais_rac_historicos do |t|
      t.belongs_to :demais_rac, null: false, index: true
      t.string :historico, null: false, limit: 100
      t.datetime :NovaDt, null: false
      t.datetime :DtAnterior, null: false
      t.text :justificativa, null: false
      t.string :NovoResponsavel, limit: 50, null: false
      t.string :AntigoResponsavel, limit: 50, null: false
      t.belongs_to :user, index: true, null: false
      t.timestamps
    end
  end
end
