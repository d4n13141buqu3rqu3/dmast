class RapsParticipantesJoinTable < ActiveRecord::Migration
  def change
    create_table :raps_participantes, :id => false do |t|
      t.integer :rap_id
      t.integer :usuario_id
    end
  end
end
