class DocsAreaJointable < ActiveRecord::Migration
  def change
    create_table :documentos_areas, :id => false do |t|
      t.integer :documento_id
      t.integer :area_id
    end
  end
end
