class AjusteAuditFunc < ActiveRecord::Migration
  def change
    drop_table :auditorias_usuarios

    create_table :auditorias_funcionarios, :id => false do |t|
      t.integer :documento_id
      t.integer :funcionario_id
    end
  end
end
