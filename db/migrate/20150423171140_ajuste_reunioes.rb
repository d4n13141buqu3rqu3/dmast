class AjusteReunioes < ActiveRecord::Migration
  def change
    drop_table :reunioes_usuarios

    create_table :reunioes_funcionarios, :id => false do |t|
      t.integer :reuniao_id
      t.integer :funcionario_id
    end
  end
end
