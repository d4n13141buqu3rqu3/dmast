class AddUsuarioToAnexoDocs < ActiveRecord::Migration
  def change
    add_reference :anexo_docs, :usuario, index: true
  end
end
