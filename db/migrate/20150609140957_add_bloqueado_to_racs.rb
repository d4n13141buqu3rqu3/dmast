class AddBloqueadoToRacs < ActiveRecord::Migration
  def change
    add_column :racs, :bloqueado, :boolean, default: false
  end
end
