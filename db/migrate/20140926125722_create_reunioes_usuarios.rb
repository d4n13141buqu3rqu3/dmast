class CreateReunioesUsuarios < ActiveRecord::Migration
  def change
    create_table :reunioes_usuarios, :id => false do |t|
    	t.integer :usuario_id
    	t.integer :reuniao_id
    end
  end
end
