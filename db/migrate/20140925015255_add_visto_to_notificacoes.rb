class AddVistoToNotificacoes < ActiveRecord::Migration
  def change
    add_column :notificacoes, :visto, :boolean
  end
end
