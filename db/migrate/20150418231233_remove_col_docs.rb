class RemoveColDocs < ActiveRecord::Migration
  def change
    remove_column :grp_homologadores, :string
    remove_column :grp_consensadores, :string
  end
end
