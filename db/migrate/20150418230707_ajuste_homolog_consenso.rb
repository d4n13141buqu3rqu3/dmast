class AjusteHomologConsenso < ActiveRecord::Migration
  def change
    add_reference :documentos, :grp_consenso, index: true
    add_reference :documentos, :grp_homologador, index: true
  end
end
