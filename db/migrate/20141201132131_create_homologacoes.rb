class CreateHomologacoes < ActiveRecord::Migration
  def change
    create_table :homologacoes do |t|
      t.belongs_to :documento, index: true
      t.boolean :homologado
      t.text :motivo_nao_homolog

      t.timestamps
    end
  end
end
