class AjustePlanos22 < ActiveRecord::Migration
  def change
    drop_table :planos_funcionarios

    create_table :planos_funcionarios, :id => false do |t|
      t.integer :planoauditoria_id
      t.integer :funcionario_id
    end
  end
end
