class CreateDocumentoLocalizacoes < ActiveRecord::Migration
  def change
    create_table :documento_localizacoes do |t|
      t.belongs_to :documento, index: true
      t.string :area
      t.string :pasta
      t.string :numero
      t.string :localizacao

      t.timestamps
    end
  end
end
