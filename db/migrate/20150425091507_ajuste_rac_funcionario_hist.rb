class AjusteRacFuncionarioHist < ActiveRecord::Migration
  def change
    remove_column :rac_historicos, :usuario_id

    add_reference :rac_historicos, :user, index: true
  end
end
