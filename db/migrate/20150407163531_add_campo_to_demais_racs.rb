class AddCampoToDemaisRacs < ActiveRecord::Migration
  def change
    add_column :demais_racs, :tipo_nc_id, :integer
    add_column :demais_racs, :desc_nc, :text
  end
end
