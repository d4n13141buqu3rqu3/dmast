class CreateDocumentoComplementars < ActiveRecord::Migration
  def change
    create_table :documento_complementars do |t|
      t.integer :doc_pai_id
      t.integer :doc_filho_id

      t.timestamps
    end
  end
end
