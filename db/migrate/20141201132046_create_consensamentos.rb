class CreateConsensamentos < ActiveRecord::Migration
  def change
    create_table :consensamentos do |t|
      t.belongs_to :documento, index: true
      t.boolean :consensado
      t.text :motivo_nao_consensamento

      t.timestamps
    end
  end
end
