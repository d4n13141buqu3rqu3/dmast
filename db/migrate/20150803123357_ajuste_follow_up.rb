class AjusteFollowUp < ActiveRecord::Migration
  def change
    drop_table(:followups)
    drop_table(:followup_emails)
  end
end
