class CreateNotificacoes < ActiveRecord::Migration
  def change
    create_table :notificacoes do |t|
      t.string :descricao
      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
