class CreateRqas < ActiveRecord::Migration
  def change
    create_table :rqas do |t|
      t.text :descricao, null: false
      t.text :proposta
      t.string :responsavel, limit: 60, null: false
      t.string :prazo, limit: 30
      t.datetime :dt, null: false
      t.belongs_to :area, index: true, null: false
      t.belongs_to :usuario, index: true


      t.timestamps
    end
  end
end
