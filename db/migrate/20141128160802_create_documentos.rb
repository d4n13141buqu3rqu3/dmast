class CreateDocumentos < ActiveRecord::Migration
  def change
    create_table :documentos do |t|
      t.string :nome, limit: 80, null: false
      t.belongs_to :modificado_por, index: true
      t.string :titulo, limit: 80
      t.belongs_to :tipoDoc, index: true
      t.string :status, limit: 30
      t.text :texto_doc
      t.integer :dias_consenso
      t.integer :dias_homologar
      t.belongs_to :usuario, index: true
      t.datetime :dt_envio_consenso
      t.datetime :dt_envio_homologacao
      t.string :sigla, limit: 30
      t.string :nome_original
      t.string :versao, limit: 50
      t.string :versao_anterior, limit: 50
      t.integer :id_doc_anterior
      t.string :link_versao_anterior, limit: 200

      t.timestamps
    end
  end
end
