class CreateRacHistoricos < ActiveRecord::Migration
  def change
    create_table :rac_historicos do |t|
      t.belongs_to :rac, index: true
      t.string :historico

      t.timestamps
    end
  end
end
