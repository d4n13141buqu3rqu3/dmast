class AjusteLocalReuniao < ActiveRecord::Migration
  def change
    remove_column :reunioes, :local_id
    add_column :reunioes, :local, :string, limit: 100, null: false
  end
end
