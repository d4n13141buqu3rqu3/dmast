class CreateAuditorias < ActiveRecord::Migration
  def change
    create_table :auditorias do |t|
      t.belongs_to :planoauditoria, index: true
      t.string :status
      t.belongs_to :area, index: true
      t.datetime :data_prevista
      t.belongs_to :auditor_lider, index: true
      t.string :duracao_prevista

      t.timestamps
    end
  end
end
