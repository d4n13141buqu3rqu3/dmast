class AjusteDocsAnexos < ActiveRecord::Migration
  def change
    remove_column :anexo_reunioes, :usuario_id
    add_reference :anexo_reunioes, :user, index: true
  end
end
