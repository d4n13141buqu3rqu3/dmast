
Area.create!([
  {descricao: "TI - Tecnologia da Informação", email: nil},
  {descricao: "PEF - Pesquisa Florestal", email: nil},
  {descricao: "CONT - Controladoria", email: nil},
  {descricao: "DRH - Recurso Humanos / Administrativo", email: nil},
  {descricao: "PDIF - Planejamento, Desenvolvimento e Informação Florestal", email: nil},
  {descricao: "COLH - Colheita Florestal", email: nil},
  {descricao: "MAT - Materiais / Almoxarfado", email: nil},
  {descricao: "MAF - Manutenção de Fazendas", email: nil},
  {descricao: "DMAST - Departamento de Meio Ambiente, Segurança e Medicina do Trabalho", email: nil},
  {descricao: "OFIC - Oficina - Manutenção em Equipamentos, Máquinas e Veículos", email: nil},
  {descricao: "REFL - Reflorestamento", email: nil},
  {descricao: "VIV - Viveiro Florestal", email: nil},
  {descricao: "FAB - Operações Fábrica", email: nil},
  {descricao: "RLC - Relações com Comunidades", email: nil},
  {descricao: "TFE - Transporte Florestal / Estradas", email: nil},
  {descricao: "DJ - Departamento Jurídico", email: nil}
])
Atividade.create!([
  {descricao: "4.1 Requisitos ambientais"},
  {descricao: "4.2 Política ambiental"},
  {descricao: "4.3 Planejamento"},
  {descricao: "4.3.1 Aspectos ambientais"},
  {descricao: "4.3.2 Requisitos legais e outros"},
  {descricao: "4.3.3 Objetivos, metas e programa(s)"},
  {descricao: "4.4 Implementação e operação"},
  {descricao: "4.4.1 Recursos, funções, responsabilidades e autoridades"},
  {descricao: "4.4.2 Compentência, treinamento e conscientização"},
  {descricao: "4.4.3 Comunição"},
  {descricao: "4.4.4. Documentação"},
  {descricao: "4.4.5 Controle de documentos"},
  {descricao: "4.4.6 Controle operacional"},
  {descricao: "4.4.7 Preparação e resposta à emergências"},
  {descricao: "4.5 verificação"},
  {descricao: "4.5.1 Monitoramento e medição"},
  {descricao: "4.5.2 Avaliação do atendimento a requisitos legais e outros"},
  {descricao: "4.5.3 Não-conformidades, ação corretiva e ação preventiva"},
  {descricao: "4.5.4 Controle de registros"},
  {descricao: "4.5.5 Auditoria interna"},
  {descricao: "4.6 Análise de administração"},
  {descricao: "SGAF_001"},
  {descricao: "SGAF_004"},
  {descricao: "SGAF_005"},
  {descricao: "SGAF_006"},
  {descricao: "SGAF_007"},
  {descricao: "SGAF_008"},
  {descricao: "SGAF_009"},
  {descricao: "SGAF_010"},
  {descricao: "SGAF_011"},
  {descricao: "SGAF_012"},
  {descricao: "SGAF_013"},
  {descricao: "SGAF_014"},
  {descricao: "SGAF_002"},
  {descricao: "SGAF_003"},
  {descricao: "teste"},
  {descricao: "tete"}
])
Filial.create!([
  {codigo: "01", nome: "Santana"},
  {codigo: "06", nome: "Porto Grande"},
  {codigo: "02", nome: "Tartarugal"}
])
Funcionario.create!([
  {chapa: "06628", nome: "Daniel Albuquerque", ativo: false, email: "daniel.albuquerque@amcel.com.br", filial_id: 1, area_id: 1, grupo_id: 4}
])

GrpConsensador.create!([
  {descricao: "MANUAL DE PROCEDIMENTOS DE COMUNICAÇÃO"},
  {descricao: "PROCEDIMENTO DE RESOLUÇÃO DE CONFLITOS E DE POSSÍVEIS PERDAS E DANOS"},
  {descricao: "PLANO DE AVALIAÇÃO E MONITORAMENTO DE IMPACTOS SOCIAIS"},
  {descricao: "PLANO DE GESTÃO SOCIAMBIENTAL"},
  {descricao: "ref"},
  {descricao: "PESQUISA FLORESTAL"},
  {descricao: "PROCEDIMENTO AMBIENTAL"},
  {descricao: "teste"}
])

GrpHomologador.create!([
  {descricao: "MANUAL DE PROCEDIMENTOS DE COMUNICAÇÃO "},
  {descricao: "PROCEDIMENTO DE GESTÃO DE CONFLITOS E DE POSSÍVEIS PERDAS E DANOS"},
  {descricao: "PLANO DE AVALIAÇÃO E DE MONITORAMENTO DE IMPACTOS AMBIENTAIS"},
  {descricao: "PLANO DE GESTÃO SOCIOAMBIENTAIS"},
  {descricao: "ref"},
  {descricao: "PESQUISA FLORESTAL"},
  {descricao: "PROCEDIMENTO AMBIENTAL"},
  {descricao: "teste"}
])

Grupo.create!([
  {descricao: "Administradores"},
  {descricao: "Auditores"},
  {descricao: "Usuários Gerais"},
  {descricao: "Consensadores e Homologadores"}
])


Planoauditoria.create!([
  {nome: "11º Periodo de Auditorias Internas", encerrado: true, prazo: 5, ano: "2012", data_inicio: nil, data_fim: nil},
  {nome: "10º Periodo de Auditorias Internas", encerrado: true, prazo: 5, ano: "2011", data_inicio: nil, data_fim: nil},
  {nome: "12º Periodo de Auditorias Internas", encerrado: true, prazo: 2, ano: "2013", data_inicio: nil, data_fim: nil},
  {nome: "13º Período de Auditorias Internas", encerrado: true, prazo: 5, ano: "2014", data_inicio: nil, data_fim: nil},
  {nome: "14º Plano de Auditoria Interna", encerrado: false, prazo: 2, ano: "2015", data_inicio: nil, data_fim: nil}
])



TipoAta.create!([
  {descricao: "Ata de Análise Crítica de Qualidade"},
  {descricao: "Ata de Gestão Operacional de Qualidade"},
  {descricao: "Ata de Melhoria Contínua"},
  {descricao: "Ata de Reunião CIPA / STN"},
  {descricao: "Ata de Reunião CIPATR / PTG"},
  {descricao: "Ata de Reunião CIPATR / Tzinho"},
  {descricao: "Ata de Análise Pós Auditoria Externa 2010"},
  {descricao: "Ata de Análise Pós Auditoria Externa 2011"},
  {descricao: "Ata de Análise Pós Auditoria Externa 2012"},
  {descricao: "Ata de Análise Pós Auditoria Externa 2013"},
  {descricao: "Ata de Análise Pós Auditoria Externa 2014"},
  {descricao: "Outras"}
])
TipoDoc.create!([
  {descricao: "Manual de Gestão Ambiental"},
  {descricao: "Manual de Gestão Operacional de Qualidade"},
  {descricao: "Manual"},
  {descricao: "Norma Técnica de Qualidade"},
  {descricao: "Plano"},
  {descricao: "Procedimento de Gestão Ambiental Florestal"},
  {descricao: "Procedimento de Qualidade"},
  {descricao: "Procedimento de Segurança e Medicina do Trabalho"},
  {descricao: "Programa de gestão Ambiental Florestal"},
  {descricao: "Programa"},
  {descricao: "Procedimento Ambiental"},
  {descricao: "Programa de gestão de Segurança do Trabalho"},
  {descricao: "Ordem de serviço"},
  {descricao: "Plano de Gestão Socioambiental"},
  {descricao: "Procedimento Socioambiental"},
  {descricao: "Plano Sociambiental"},
  {descricao: " Plano de Gestão de Saúde e Segurança do Trabalho"},
  {descricao: "Sistema de Gestão Ambiental Florestal"}
])
TipoNc.create!([
  {sigla: "AMB", descricao: "Ambiental"},
  {sigla: "FSC", descricao: "FSC"},
  {sigla: "QLD", descricao: "Qualidade"},
  {sigla: "SEG", descricao: "Segurança"},
  {sigla: "CLFOR", descricao: "CERFLOR - PEFC"}
])
TipoReclamacao.create!([
  {descricao: "RE - Reclamação Externa."},
  {descricao: "RI - Reclamação Interna."},
  {descricao: "RS - Reclamação de Segurança."}
])
User.create!([
  {provider: "google_oauth2", uid: "113062066582658460772", name: "Alcione Maciel", oauth_token: "ya29.wAEQQIgs4k9ZaRDWNQqdjjYbWUymrsZqxrt5XQwsjnrYkHpbdTa4gUx7oFjQ1tiT9_nB4g", photo: "https://lh6.googleusercontent.com/-fer8e_MsbW0/AAAAAAAAAAI/AAAAAAAAABI/q_QbtSRiDtw/photo.jpg?sz=50", email: "alcione.maciel@amcel.com.br", oauth_expires_at: "2015-07-30 18:59:05"},
  {provider: "google_oauth2", uid: "109162995356901217367", name: "Carlos Goncalves", oauth_token: "ya29.1AHJ0B_RmPZOYCNZ2JL5IvjFJdHzYzSYC38LbFZAdOK0ybPkLcpdlUD1zxHmrtJN1xa4", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "carlos.goncalves@amcel.com.br", oauth_expires_at: "2015-08-19 11:00:33"},
  {provider: "google_oauth2", uid: "104212510380005740171", name: "Eduardo Marques", oauth_token: "ya29.0wFBcMjstkfZlOBpjX2OOwmBj6_pILncQ8ee1EQlHZfDGhR8tW6GW4dxSfRQNuBH4rma", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "eduardo.marques@amcel.com.br", oauth_expires_at: "2015-08-18 15:25:13"},
  {provider: "google_oauth2", uid: "113027886028580832952", name: "Aliny Silva", oauth_token: "ya29.1AGrD2Vb7hCZKrYm7_pDYER8kDT-AH60EBhMsQbU2nOHTkC9GPmpHYnnmi8toZHsJLCr", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "aliny.silva@amcel.com.br", oauth_expires_at: "2015-08-19 12:13:03"},
  {provider: "google_oauth2", uid: "109170492985458397275", name: "Marcelo Muniz", oauth_token: "ya29.0gF_mHIIEnW90Bxcuo5WcGIXovEp78zJKz0aSGfFwpbwazvAXZreL_wCx1s3rjMB8cn1", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "marcelo.muniz@amcel.com.br", oauth_expires_at: "2015-08-17 20:00:21"},
  {provider: "google_oauth2", uid: "105363120927459215991", name: "Ivan Gama", oauth_token: "ya29.xAFlvUMjJ3pwGzM26q4JccdNeZUNSWBT3WDZhv3SOpGToJi40E82I3OwdzB0wGCitgkJ", photo: "https://lh3.googleusercontent.com/-Smx4HFtdhlw/AAAAAAAAAAI/AAAAAAAAACU/AiOHrnFPR5A/photo.jpg?sz=50", email: "ivan.gama@amcel.com.br", oauth_expires_at: "2015-08-03 18:27:58"},
  {provider: "google_oauth2", uid: "114712577625811290441", name: "Eduardo Marques", oauth_token: "ya29.0gHNLOBhSS-8dLXdANl7V-9NzoOJlut73Fh8ip8IGhmaRmEU_MDNtwF8AWlnIqpLAPOt", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "marquesmcp@gmail.com", oauth_expires_at: "2015-08-17 01:55:50"},
  {provider: "google_oauth2", uid: "101616089689359035254", name: "Ana Neves", oauth_token: "ya29.0wEazpUMAdvYa2te-eqehzCPxHWHd79s5V5MAQe-qZmjXAof0Xq42vXBw4M_kBUT6IrD", photo: "https://lh4.googleusercontent.com/-RRDE-xcQ8-g/AAAAAAAAAAI/AAAAAAAAAAo/T1LkSaHF8f4/photo.jpg?sz=50", email: "ana.neves@amcel.com.br", oauth_expires_at: "2015-08-18 13:15:02"},
  {provider: "google_oauth2", uid: "116368660761654894685", name: "Alvaro Cambuy", oauth_token: "ya29.ngGfHdAsDwQqIFX4Jov3TgGo5mOJB2NMlzilgwkyd5t53IxZ2G_4hI5Fw5L03h7jeIBW8HqUvjW31Q", photo: "https://lh5.googleusercontent.com/-Li7oWlqh5Rc/AAAAAAAAAAI/AAAAAAAAAMQ/KNmgEhgJOiY/photo.jpg?sz=50", email: "alvaro.cambuy@amcel.com.br", oauth_expires_at: "2015-06-26 16:24:07"},
  {provider: "google_oauth2", uid: "109505468543848079034", name: "Daniel Albuquerque", oauth_token: "ya29.0wGPWrGcta_sqsJPPeCB725mjYSYcxFApYOotFBaHUoRUR5nRSqee2Hx1JU", photo: "https://lh4.googleusercontent.com/-jUYz84xEcd8/AAAAAAAAAAI/AAAAAAAAAL8/I2toCjr8gOE/photo.jpg?sz=50", email: "daniel.alb.querque@gmail.com", oauth_expires_at: "2015-08-18 17:06:09"},
  {provider: "google_oauth2", uid: "104385777415053935744", name: "Jose Pinheiro", oauth_token: "ya29.0gE7Iot4dAffcRupNJfB1mJTKIILbD48S-fnNKWNpwqovpM9xaFlouQkNvboHVbwG65L", photo: "https://lh4.googleusercontent.com/-wjRz3QzR54E/AAAAAAAAAAI/AAAAAAAAA5Q/Jt0m-AER3Zs/photo.jpg?sz=50", email: "jose.pinheiro@amcel.com.br", oauth_expires_at: "2015-08-17 20:12:10"},
  {provider: "google_oauth2", uid: "101331439495008770925", name: "Aldinete Pinheiro", oauth_token: "ya29.1AEuBYjXcBeRcSYHo8SKnou96N--G04Etc4nbHuaR64kbGJoxyLuRwE0GNaNFyFqD9K5BQ", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "aldinete.pinheiro@amcel.com.br", oauth_expires_at: "2015-08-19 12:46:11"},
  {provider: "google_oauth2", uid: "115203559552986748102", name: "Valdir Araujo", oauth_token: "ya29.1AESn1avSAKIYcfVUmrrHlX6ukTQRDk-qZenzkBptsDqfZBlXOOdK073qv5RhmHoxE3p", photo: "https://lh5.googleusercontent.com/-jvWiG3rjR54/AAAAAAAAAAI/AAAAAAAAACA/4zFHGujRekg/photo.jpg?sz=50", email: "valdir.araujo@amcel.com.br", oauth_expires_at: "2015-08-19 14:27:54"},
  {provider: "google_oauth2", uid: "115660010854167115658", name: "Janira Farias", oauth_token: "ya29.1AHukhG8oQnic5zFHQ5TDozBPLzhxw89JnCblU7V4Hs0wmE8JIfBQgRKeD1k9iOCkA3Z", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "janira.farias@amcel.com.br", oauth_expires_at: "2015-08-19 18:25:14"},
  {provider: "google_oauth2", uid: "105601635961533912569", name: "Geraldo Melo", oauth_token: "ya29.0wFC1gtXtgWUysRP_fU6qPdUg2u7Jsbbx2wIVzBY73465wXmgXPB7Z7sMuuLyiTIobeS", photo: "https://lh3.googleusercontent.com/-tbikn4mqKqs/AAAAAAAAAAI/AAAAAAAAAEU/wauzkocBBDA/photo.jpg?sz=50", email: "geraldo.melo@amcel.com.br", oauth_expires_at: "2015-08-18 18:02:55"},
  {provider: "google_oauth2", uid: "107507251955754611057", name: "Newton Araujo", oauth_token: "ya29.0gE8PL3DQhE_o7dcFBIeBjbxlvGGF_pLJOEbNi5ab6wRRIKYD17nwitiq3X6b98G2oT9", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "newton.araujo@amcel.com.br", oauth_expires_at: "2015-08-17 19:41:26"},
  {provider: "google_oauth2", uid: "102050342872221501729", name: "Andre Costa", oauth_token: "ya29.0wF_e09REiFnuDWPjtAIK7prJCnqgulB_mQnrMkG6fwClEUjA8YUBKRYGKbDJyvLEIIXmQ", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "andre.costa@amcel.com.br", oauth_expires_at: "2015-08-18 20:36:38"},
  {provider: "google_oauth2", uid: "116958850411324156038", name: "Rogerio Cardoso", oauth_token: "ya29.0wHhs7NxXRgqPQD-MzVcFuZ5yBI6vt__zbzN6fYfpF5TMaDgTTMtwFruPLL6QqQJ78wq", photo: "https://lh6.googleusercontent.com/-UdEQcDLloOo/AAAAAAAAAAI/AAAAAAAAABM/22nUkXPZCLU/photo.jpg?sz=50", email: "rogerio.cardoso@amcel.com.br", oauth_expires_at: "2015-08-18 13:46:13"},
  {provider: "google_oauth2", uid: "102966472635387271095", name: "Cristiane Morais", oauth_token: "ya29.0wEBkHSby-6t43ssq-17_i-UXfs_cTRtJ6omzEc5zhH7IAukTvpi5sNzskgY5Jndkc8n6Q", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "cristiane.morais@amcel.com.br", oauth_expires_at: "2015-08-18 13:48:47"},
  {provider: "google_oauth2", uid: "103462568057047688146", name: "Marcia Souza", oauth_token: "ya29.0wEdvhwskDLfZceppu_ok_zo1rzo9Ckmw-Q8w0rtxOkvY5MzEHwiA4ocKLyJDFDix00z", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "marcia.souza@amcel.com.br", oauth_expires_at: "2015-08-18 14:00:49"},
  {provider: "google_oauth2", uid: "108157820739148676175", name: "Daniel Albuquerque", oauth_token: "ya29.1QE5aKK4ISa16-KMEUMtT9nJg7GrbLRlecQdL7gbebqv7m1hh1nAxHDUrUPN0XHbm6dl", photo: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50", email: "daniel.albuquerque@amcel.com.br", oauth_expires_at: "2015-08-20 12:28:15"}
])
